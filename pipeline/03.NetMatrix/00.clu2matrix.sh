#!/usr/bin/bash

set -euox pipefail

inputfile=$1
prefix=$2
output=`dirname $inputfile`

#wp=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/clu_to_matrix
wp=$(dirname $(readlink -f "$0"))

python3 $wp/01.clu2tb.py $inputfile

/usr/bin/Rscript $wp/02.tb2matrix.R $prefix

sed -i -e 's/clu://g' -e 's/|/,/g' ${prefix}.matrix

python3 $wp/03.matrix_trans.py $prefix $output
