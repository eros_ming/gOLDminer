#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File       :   constant.py
@Time     :   2022/01/19 22:01:46
@Author  :   Xie Xiaoming
@Version  :   1.0
@Contact :   erosminer@icloud.com
@License :   (C)Copyright 2017-2021, All rights reserved
@Desc    :   None
'''

# here put the import lib

import sys


class _const:
    class ConstError(TypeError):
        pass

    class ConstCaseError(ConstError):
        pass

    def __setattr__(self, name, value):
        if self.__dict__.has_key(name):
            raise self.ConstError("Can't change const.{}".format(name))
        if not name.isupper():
            raise self.ConstCaseError(
                "const name {} is not all uppercase".format(name))
        self.__dict__[name] = value


sys.modules[__name__] = _const()

import const
const.MY_CONSTANT = 1
const.MY_SECOND_CONSTANT = 2
