#!/usr/bin/bash

#SBATCH --job-name=GetNetMatrix
#SBATCH --nodes=1
#SBATCH --ntasks=14
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=2
#SBATCH --mem=20gb

# set -euox pipefail

#data
WorkPath=$HOME/projs/GeneODL/pipeline/03.NetMatrix #工作目录，和01 WorkPath一样的的数据输出目录保持一致
CollinearPairsDB=$HOME/projs/GeneODL/pipeline/00.PrepData/CollinearPairsDB_20220122  #两两比较数据目录，和02 wp一样的的数据目录保持一致

TBFILE=$1
#$HOME/projs/GeneODL/pipeline/00.PrepData/GroupTB/all_wo_ak58.tb #Change to the same genome list file path as 01
coltmp=`basename $TBFILE | cut -d "." -f 1`
for genome_from in `cat $TBFILE | awk '{print $1"_"$2}' | tr '\n' ' '`;do
	for genome_to in `cat $TBFILE | awk '{print $1"_"$2}' | tr '\n' ' '`;do
		file=$CollinearPairsDB/${genome_from}-${genome_to}.link
		if [ ! -x "$file" ]; then
			cat $file | awk '$4 != 0{print $1":"$2,$3":"$4,$5}' >> $WorkPath/${coltmp}.tmp
		else
			echo $genome_from $genome_to >> $WorkPath/${coltmp}.wo
		fi
	done
done

#modules
module load mcl/12-068  #Load the tool
CLU2MAT=$WorkPath/00.clu2matrix.sh

prefix=TEST  #Create the output result folder and set the output file prefix	
ResultsDir=$WorkPath/${prefix}_`date +%Y%m%d`  #第三步结果输出目录

# for mcl_i in 1.5 2 3 4;do
mcl_i=2
mkdir -p $ResultsDir/process_data_${mcl_i}
cd $ResultsDir/process_data_${mcl_i}

#mcl
# mcxload -abc $WorkPath/${coltmp}.tmp --stream-mirror -write-tab ${coltmp}.tab -o ${coltmp}.mci

# mcl ${coltmp}.mci -I $mcl_i -o ${coltmp}.I

# mcxdump -icl ${coltmp}.I -tabr ${coltmp}.tab -o $ResultsDir/NetMatrix_${mcl_i}_$coltmp

mcl $WorkPath/${coltmp}.tmp --abc -I 2 -o $ResultsDir/NetMatrix_${mcl_i}_$coltmp

#backbone CLU2MAT
cd $ResultsDir
sh $CLU2MAT  $ResultsDir/NetMatrix_${mcl_i}_$coltmp ${mcl_i}_$coltmp
# done