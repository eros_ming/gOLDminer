#!/usr/bin/bash
# odlQ

database=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015

size="$database"/all_20220112_NewID.size
clu="$database"/all_20220112_NewID.clu
chr="$database"/all_20220112_NewID.chr
mat="$database"/all_20220112_NewID.matrix

geneLst="$1"
rm -f /dev/shm/tmp.size
for gene in `echo "$geneLst" | tr ',' ' '`;do
    cluid=`grep -w "$gene" "$clu" | awk '{print $1}'`
    grep -w "$cluid" "$size" >> /dev/shm/tmp.size
done
(head -n 1 "$size" ; sort /dev/shm/tmp.size | uniq ) |column -t | less -SN
