#!/usr/bin/env R
args<-commandArgs(T)

#定义单个基因组鉴定函数便于多个基因组并行分析 22.05.18 加入 singleton
TDGFinder <- function(i){
  ##加载包
  library(igraph)     #igraph 用于进行模块发现
  library(reshape2)   #可以将list转成dataframe格式
  library(ggplot2)
  library(dplyr)
  
  ##数据路径
  path_homo <- "/data/user/chenym/data_fordatabase/homolog_2021/"
  path_bed <- "/data/user/chenym/data_fordatabase/rawdata/"
  
  genome_from <- genomes[i]
  genome_to <- genome_from
  bedfile <- paste0(path_bed,genome_from,".bed")
  singleton <-  paste0(path_homo,genome_from,"_",genome_to,"/",genome_from,"_",genome_to,"itself.singleton")
  homologsfile <- paste0(path_homo,genome_from,"_",genome_to,"/",genome_from,"_",genome_to,"itself.one2many")

  if (file.exists(homologsfile) & file.exists(bedfile) & file.exists(singleton)) {
    ##读入数据
    gene_one2many <- read.table(homologsfile,header = F,col.names = c("from","to","score"))
    gene_singleton <- read.table(singleton,header = F,col.names = c("from")) #TODO 增加singleton部分基因
    gene_singleton$to <- gene_singleton$from
    gene_singleton$score <- rep(100,nrow(gene_singleton)) 
    gene_pair_data <- rbind(gene_one2many, gene_singleton) 
    gene_bed <- read.table(bedfile,header = F,col.names = c("chr","start","end","id","none","chain"))

    ##筛选同染色体基因对
    names(gene_pair_data) <- c("id","to","score")
    gene_pair_data_bef <- dplyr::left_join(gene_pair_data,gene_bed,by = "id")[,c(1,2,4)]
    
    names(gene_pair_data_bef) <- c("from","id","chr")
    gene_pair_data_aft <- dplyr::left_join(gene_pair_data_bef,gene_bed,by = "id")[,c(1:4)]
    
    gene_pair_data <- gene_pair_data_aft
    names(gene_pair_data) <- c("from","to","from_chr","to_chr")
    gene_pair_chr_match <- droplevels(subset(gene_pair_data,gene_pair_data$from_chr == gene_pair_data$to_chr))# 限制条件1 {同染色体}
    
    ##清理数据
    rm(gene_pair_data_bef,gene_pair_data_aft,gene_one2many,gene_singleton)

    ##建立基因的index
    gene_num_by_chr <- table(gene_bed$chr)
    index <- list()
    for (i in (1:length(gene_num_by_chr))) {
    index[[i]] <- c(1:gene_num_by_chr[i])
    }
    gene_bed$index <- unlist(index)

    ##计算基因间距离
    gene_bed$cen <- round((gene_bed$start + gene_bed$end)/2)
    gene_bed <- gene_bed[c(4,1:3,7:8,6)]

    ##利用dplyr快速合并基因对与bed信息
    from_data <- data.frame(from = gene_bed$id,f.start = gene_bed$start,f.end = gene_bed$end,
                          f.index = gene_bed$index,f.cen = gene_bed$cen)
    to_data <- data.frame(to = gene_bed$id,t.start = gene_bed$start,t.end = gene_bed$end,
                        t.index = gene_bed$index,t.cen = gene_bed$cen)

    gene_pair_data_pre <- left_join(gene_pair_chr_match,from_data,by = "from")
    gene_pair_data_new <- left_join(gene_pair_data_pre,to_data,by = "to")

    #利用距离筛选数据，d < 5ind
    gene_pair_data_new$dis_cen <- abs(gene_pair_data_new$f.cen - gene_pair_data_new$t.cen)
    gene_pair_data_new$dis_ind <- abs(gene_pair_data_new$f.index - gene_pair_data_new$t.index)
    data_for_igraph <- droplevels(subset(gene_pair_data_new,gene_pair_data_new$dis_ind <= d))# 限制条件2 {基因对间的距离}

    ##清理数据
    rm(gene_pair_data_new,gene_pair_chr_match,gene_pair_data,from_data,to_data)

    ##创建igraph对象g
    g <- graph_from_data_frame(data_for_igraph, vertices = gene_bed)  

    #利用节点属性计算边属性
    ends_g <- ends(g,E(g))

    E(g)$dis_cen <- abs(V(g)[ends_g[,1]]$cen - V(g)[ends_g[,2]]$cen)
    E(g)$dis_ind <- abs(V(g)[ends_g[,1]]$index - V(g)[ends_g[,2]]$index)
    E(g)$dis <- (abs(V(g)[ends_g[,2]]$start - V(g)[ends_g[,1]]$end) + abs(V(g)[ends_g[,1]]$start - V(g)[ends_g[,2]]$end))/2
    E(g)$weight <- 1/E(g)$dis

    #The edge weights. Larger edge weights increase the probability that an edge is selected by the random walker. 
    #In other words, larger edge weights correspond to stronger connections.

    #利用随机游走模型通过距离权重发现基因簇
    wcg <- walktrap.community(g, weights = E(g)$weight, steps = 4, merges = TRUE, modularity = TRUE, membership = TRUE)
    clu_from_g <- wcg[sizes(wcg) >= 1]
    #存成结构化的json结构
    #cat(toJSON(clu_from_g, pretty = TRUE), file = paste0(path_workF,"/result_",d,"/",genome_from,".json"), fill = FALSE, labels = NULL, append = FALSE)

    #存成易读的结构
    cluster <- data.frame(gene = wcg$names,membership = wcg$membership)
    df_clu <- cluster[order(cluster$membership),] 

    names(gene_bed) <- c("gene","chr","start","end","index","cen","chain")
    new_df_clu <- dplyr::left_join(df_clu,gene_bed,by = "gene")[,c(3,4,5,1,2,8,6)]
    last_df_clu <- new_df_clu[order(new_df_clu$chr, new_df_clu$index),]
    clusize <- table(new_df_clu$membership)
    tdif <- ifelse(clusize >= 2,"td","ntd")
    last_df_clu$type <- tdif[last_df_clu$membership]
    
    write.table(last_df_clu,file = paste0(path_workF,"/result_",d,"/",genome_from,".cludb"),quote = F,row.names = F,col.names = F)
    cleanlist <- function(i,list) {
      wcg <- list
      k <- i
      gene <- wcg[[k]][1]
      len <- length(wcg[[k]])
      gene_end <- wcg[[k]][len]
      cluster <- paste(wcg[[k]], collapse = ",")
      df <- data.frame(gene = gene, gene.e = gene_end, length = len, cluster = cluster)
      return(df)
    }

    new_li <- lapply(1:length(clu_from_g), cleanlist, list = clu_from_g)
    l <- melt(new_li, id.vars = c("gene","gene.e","cluster"))[,c(1,2,3,5)]
    clu_li <- dplyr::left_join(l,gene_bed,by = "gene")[,c(1:5)]
    last_df_cluli <- clu_li[order(clu_li$chr, -clu_li$value),]
    write.table(last_df_cluli,file = paste0(path_workF,"/result_",d,"/",genome_from,".clu"),quote = F,row.names = F,col.names = F)

    clucount <- clu_li %>%
    group_by(chr) %>%
    summarise(
      tandem_clu = sum( value >= 2)
    )
    
    tdcount <- last_df_clu %>%
    group_by(chr) %>%
    summarise(
      total_gene=n(),
      tandem_gene = sum( type == 'td'),
      none_tandem_gene = sum( type == 'ntd'),
      percent = tandem_gene/total_gene
    )

    bothcount <- dplyr::left_join(clucount,tdcount,by = "chr")
    
    subg <- dplyr::filter(subG,Gm == as.character(genome_from))
    
    for (i in 1:length(subg$subG)) {
      chrlist <- unlist(strsplit(as.character(subg$CHRlist[i]),split = ","))
      
      Clu <- dplyr::filter(last_df_clu,(chr %in% chrlist))
      Cluli <- dplyr::filter(last_df_cluli,(chr %in% chrlist))
      write.table(Clu,file = paste0(path_workF,"/result_",d,"/",genome_from,"_",subg$subG[i],".cludb"),quote = F,row.names = F,col.names = F)
      write.table(Cluli,file = paste0(path_workF,"/result_",d,"/",genome_from,"_",subg$subG[i],".clu"),quote = F,row.names = F,col.names = F)
      
      count <- dplyr::filter(bothcount,(chr %in% chrlist))
      m <- apply(count[,c(-1,-6)],2,sum)
      s <- paste0(genome_from,"_",subg$subG[1])
      e <- round(m[3]/m[2],4)
      write.table(t(data.frame(var = c("SubGenome",names(m),"TDGpercent"),value = c(s,m,e))),
                  file = paste0(path_workF,"/result_",d,"/",genome_from,"_",subg$subG[i],".stat"),quote = F,row.names = F,col.names = F)
    }
  }else{
    message("There is no Adequate Data for Tandem Duplicated Genes Determine! Exiting ......")
  }
}

##设置路径
path_workF <- as.character(args[1])
## 参数控制
d <- as.numeric(args[2])
# 创建结果存储目录
system(paste0("mkdir -p ",path_workF,"/result_",d))

## 读入基因组列表
subG.tb <- as.character(args[3]) #"/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/subGmspbyChr.tb"
subG <- read.table(subG.tb,col.names = c("Gm","subG","CHRlist"))
genomes <- unique(subG$Gm)

#genomes_for_connect_ever_two_genome <- paste0(subG$Gm,"_",subG$subG)

## R 并行
library(parallel)
l <- length(genomes)
x <- 1:l
if (l >= 14) {
  cl <- makeCluster(14)
}else{
  cl <- makeCluster(l)
}
clusterExport(cl, c("path_workF", "d", "subG", "genomes"), environment())
parSapply(cl, x, TDGFinder)
stopCluster(cl)
