#!/bin/bash

#SBATCH --job-name=TDGFinder
#SBATCH --nodes=1
#SBATCH --ntasks=14
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=20gb

/usr/bin/Rscript TDGFinder.R \
  /data2/user2/xiexm/projs/GeneODL/pipeline/01.Clustering \
  5 \
  $1
  #/data3/user3/publicdata/xxm_tmp/xxm/data/raw/subGmspbyChr.tb
