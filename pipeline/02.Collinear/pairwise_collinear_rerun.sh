#!/bin/bash

#SBATCH --job-name=pairwise_comp
#SBATCH --nodes=1
#SBATCH --ntasks=14
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=20gb
#SBATCH --nodelist=wheat[3]

set -exou pipefail
reruntb=$1
#
matchClu(){
    subG=$1  
    gm1=`echo $subG | cut -d- -f1`
    gm2=`echo $subG | cut -d- -f2`

    wp="$HOME"/projs/GeneODL/pipeline/00.PrepData/CollinearPairsDB_20220122/
    if [ $gm1 != $gm2 ];then
        /usr/bin/Rscript "$HOME"/projs/GeneODL/pipeline/02.Collinear/pairwise_link.R $gm1 $gm2
    fi

}
#
export -f matchClu
#
subGlst=`cat $reruntb`
parallel -j 14 matchClu {} ::: $subGlst
