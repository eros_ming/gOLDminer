#!/bin/bash

#SBATCH --job-name=pairwise_comp
#SBATCH --nodes=1
#SBATCH --ntasks=14
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=20gb
#SBATCH --nodelist=wheat[3]

set -exou pipefail
tb=$1
echo "["`date +%Y-%m-%d,%H:%M:%S`"] Start match Clusters between two Genome"
#
matchClu(){
    subG=$1  
    gm1=`echo $subG | cut -d- -f1`
    gm2=`echo $subG | cut -d- -f2`

    wp="$HOME"/projs/GeneODL/pipeline/00.PrepData/CollinearPairsDB_20220122/
    if [ ! -f "$wp/${gm1}-${gm2}.link" ];then
        if [ $gm1 != $gm2 ];then
            echo "开始进行两两基因组共线性基因簇关联，参照基因组为${gm1},目标基因组为${gm2}。"
            #路径结尾最好带上/
            /usr/bin/Rscript "$HOME"/projs/GeneODL/pipeline/02.Collinear/pairwise_link.R $gm1 $gm2
            #  "$wp"/00.PrepData/CollinearPairsDB_20220122/ \
            #  "$wp"/00.PrepData/ClusterDB_20210821/ \
            #  /data/user/chenym/data_fordatabase/microcollinearity_file1/ \
            #  /data/user/chenym/shiny_TGT_database/singleBest_colblocks/
            echo "["`date +%Y-%m-%d,%H:%M:%S`"] Finish match Clusters between $gm1 and $gm2."
        fi
    fi

}
#
export -f matchClu
#
gmlst=`cat $tb | awk '{print $1"_"$2}' | tr '\n' ' '`
subGlst=`for gmf in $gmlst;do for gmt in $gmlst;do echo ${gmf}-$gmt; done; done`
parallel -j 14 matchClu {} ::: $subGlst
#
echo "["`date +%Y-%m-%d,%H:%M:%S`"] Finish match Clusters between two Genome"

#sh selfnoed.sh $tb

