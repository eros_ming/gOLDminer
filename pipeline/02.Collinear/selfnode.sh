#!/usr/bin/bash
#
wp=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/result_5
#for clu in $wp/*_*.clu;do
while read line;do
	gm=`echo $line | awk '{print $1}'`
	clu=$wp/"$gm"_*.clu
	echo $clu
	name=`basename $clu | cut -d '.' -f1`
    awk -vgm=$gm '{print gm"\t"NR"\t"gm"\t"NR}' $clu > /data2/user2/xiexm/projs/GeneODL/pipeline/00.PrepData/CollinearPairsDB_20220122/${name}-${name}.link
done < $1
