#!/usr/bin/env python3
from hashlib import blake2s
from operator import le, sub
import sys
import os


cluid = sys.argv[1]
expansion = sys.argv[2]
clufile = sys.argv[3]

datadir = "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/"

clu = datadir + "all_2021-10-15225207.clu"
size = datadir + "all_2021-10-15225207.size"

cmd = '(head -n 1 ' + clu + ';grep -w ' + cluid + ' ' + clu + ') > ' + clufile
os.system(cmd)

#spec_list = []
spec_order_list = [10,22,34,69,68,50,70,71,74,76] # range(77)
clu_dict = {}

def  get_bed(spec, gene_list, expansion):
    data_path = "/data/user/chenym/data_fordatabase/rawdata/"
    bed = data_path + spec.split("_")[0] +".bed"
    tmp_dict = {}

    new_gene_lst = []

    with open(bed) as bed_file:
        gene_bed = bed_file.readlines()
        i = 0
        gene_lst = []
        order_lst =[]
        for g in gene_bed:
            bed_record = g.strip().split("\t")
            gene = bed_record[3]
            gene_lst.append(gene)
            if gene in gene_list:
                order_lst.append(i)
            sub_tmp_dict ={}
            sub_tmp_dict["spec"] = spec
            sub_tmp_dict["chr"] = bed_record[0]
            sub_tmp_dict["start"] = bed_record[1]
            sub_tmp_dict["end"] = bed_record[2]
            sub_tmp_dict["strand"] = bed_record[5]
            tmp_dict[gene] = sub_tmp_dict
            i += 1

        if len(order_lst) >= 1:
            min_ind = min(order_lst) - int(expansion)
            max_ind = max(order_lst) + int(expansion) + 1
            new_gene_lst = gene_lst[min_ind:max_ind]

            j = 0
            for gene in new_gene_lst:
                j += 1
                if gene in gene_list:
                    tag = "HoGs"
                else:
                    tag = "NotHoGs"

                # print("Triodls",tmp_dict[gene]['spec'],tmp_dict[gene]['start'],tmp_dict[gene]['end'],
                # tmp_dict[gene]['strand'],"CDS",tmp_dict[gene]['spec']+str(j).zfill(4),
                # 'null','null',tag,'NA','0',str(int(tmp_dict[gene]['end']) - int(tmp_dict[gene]['start'])),
                # "0.433333",gene,"LLL",tmp_dict[gene]['spec']+str(j).zfill(4),)

    return(new_gene_lst)

def get_link(from_spec, to_spec, from_gene_lst, to_gene_lst):
    data_path = "/data/user/chenym/data_fordatabase/microcollinearity_file1/" 
    blocks = data_path + from_spec + '.' + to_spec + '.i1.blocks'
    collinearity_dict = {}
    with open(blocks) as block_file:
        data = block_file.readlines()
        for line in data:
            pairs = line.strip().split("\t")
            collinearity_dict[pairs[0]] = pairs[1]
    links = {}
    for gene in from_gene_lst:
        try:
            to_gene = collinearity_dict[gene]
            if to_gene in to_gene_lst:
                links[gene] = to_gene
        except KeyError:
            continue
    return(links)

with open(clufile) as f:
    spec = f.readline().strip().split("\t")
    clusters = f.readline().strip().split("\t")
    gene_lst_dict = {}
    spec_list = []
    for ogs in spec_order_list:
        sub_spec = spec[ogs]
        spec_list.append(sub_spec)
        gene_list = clusters[ogs].split(",")
        new_gene_lst =  get_bed(sub_spec, gene_list, expansion)
        gene_lst_dict[sub_spec] = new_gene_lst
    
    for i in range(9):
        from_spec = spec_list[i]
        to_spec = spec_list[i+1]
        from_gene_lst = gene_lst_dict[from_spec]
        to_gene_lst = gene_lst_dict[to_spec]
        links = get_link(from_spec, to_spec, from_gene_lst, to_gene_lst)
        for k in links:
        # [1] "seq_id"  "length"  "start"   "end"     "strand" 
        # [6] "seq_id2" "length2" "start2"  "end2"
            seq_id = k
            seq_id2 = links[k]
            print(seq_id,seq_id2,from_spec,to_spec)

    for i in list(reversed(range(1,10))):
        from_spec = spec_list[i]
        to_spec = spec_list[i-1]
        from_gene_lst = gene_lst_dict[from_spec]
        to_gene_lst = gene_lst_dict[to_spec]
        links = get_link(from_spec, to_spec, from_gene_lst, to_gene_lst)
        
        for k in links:
        # [1] "seq_id"  "length"  "start"   "end"     "strand" 
        # [6] "seq_id2" "length2" "start2"  "end2"
            seq_id = k
            seq_id2 = links[k]
            print(seq_id,seq_id2,from_spec,to_spec)