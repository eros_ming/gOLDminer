#!/usr/bin/bash
#!/usr/bin/bash

#SBATCH --job-name=GenesLinks
#SBATCH --nodes=1
#SBATCH --ntasks=10
#SBATCH --cpus-per-task=1
#SBATCH --mem=20gb
#SBATCH --output=%j.o
#SBATCH --error=%j.e

while read line;do
    cluid=`echo $line | awk '{print $1}'`
    clufile=`mktemp -u /data2/user2/xiexm/tmp/tmp.XXXXXX`
    python3 get_genesV2.0.py "$cluid" 2 "$clufile" > "$cluid".genes
    python3 get_links.py "$cluid" 2 "$clufile" > "$cluid".links
done < /data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/all_2021-10-15225207.clu

#/usr/bin/Rscript plot_ggg.r "$cluid"