#!/usr/bin/env python3
from operator import sub
from re import L
import sys
import os


cluid = sys.argv[1]

datadir = "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/"
clu = datadir + "all_2021-10-15225207.clu"
size = datadir + "all_2021-10-15225207.size"

cmd = '(head -n 1 ' + clu + ';grep -w ' + cluid + ' ' + clu + ') > /dev/shm/tmp.clu'
os.system(cmd)

#spec_list = []
spec_order_list = [10,22,34,69,68,50,70,71,74,76] # range(77)
clu_dict = {}

with open('/dev/shm/tmp.clu') as f:

    def get_bed(spec,gene_list):
        data_path = "/data/user/chenym/data_fordatabase/rawdata/"
        bed = data_path + spec.split("_")[0] +".bed"
        tmp_dict = {}
        mi = []
        ma = []
        with open(bed) as bedf:
            gbed = bedf.readlines()
            for g in gbed:
                bed_record = g.strip().split("\t")
                gene = bed_record[3]
                if gene in gene_list:
                    sub_tmp_dict ={}
                    sub_tmp_dict["chr"] = bed_record[0]
                    sub_tmp_dict["start"] = bed_record[1]
                    sub_tmp_dict["end"] = bed_record[2]
                    sub_tmp_dict["strand"] = bed_record[5]
                    tmp_dict[gene] = sub_tmp_dict
                    mi.append(int(bed_record[1]))
                    ma.append(int(bed_record[2]))
                    #print("ODLS",spec,bed_record[1],bed_record[2],bed_record[5],"CDS",gene)
        if len(ma) != 0:
            Len = max(ma) - min(mi)
        else:
            Len = 0
        return(Len)

    spec = f.readline().strip().split("\t")
    clusters = f.readline().strip().split("\t")
    for ogs in spec_order_list:
        sub_spec = spec[ogs]
        #spec_list.append(sub_spec)
        gene_list = clusters[ogs].split(",")
        #print(sub_spec, gene_list)
        clu_dict[sub_spec] = gene_list
        Len = get_bed(sub_spec, gene_list)
        print("ODLS",sub_spec,"ODLS:"+cluid,Len)
        #print(ogs, "\n", dict_tmp ,"\n_____________________________________________")    
#print(clu_dict)



