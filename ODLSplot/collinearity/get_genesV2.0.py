#!/usr/bin/env python3
from operator import sub
import sys
import os
from turtle import width


cluid = sys.argv[1]
expansion = sys.argv[2]
clufile = sys.argv[3]
width = int(sys.argv[4])
length = int(sys.argv[5])


datadir = "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/"
clu = datadir + "all_2021-10-15225207.clu"
size = datadir + "all_2021-10-15225207.size"

#cmd = 'mktemp /data2/user2/xiexm/tmp/tmp.XXXXXX'

cmd = '(head -n 1 ' + clu + ';grep -w ' + cluid + ' ' + clu + ') > ' + clufile
os.system(cmd)

#spec_list = []
spec_order_list = [10,22,34,69,68,50,70,71,74,76] # range(77)
clu_dict = {}

def get_bed(spec,gene_list,expansion):
    data_path = "/data/user/chenym/data_fordatabase/rawdata/"
    bed = data_path + spec.split("_")[0] +".bed"
    tmp_dict = {}
    with open(bed) as bed_file:
        gene_bed = bed_file.readlines()
        i = 0
        gene_lst = []
        order_lst =[]
        for g in gene_bed:
            bed_record = g.strip().split("\t")
            gene = bed_record[3]
            gene_lst.append(gene)
            if gene in gene_list:
                order_lst.append(i)
            sub_tmp_dict ={}
            sub_tmp_dict["spec"] = spec
            sub_tmp_dict["chr"] = bed_record[0]
            sub_tmp_dict["start"] = bed_record[1]
            sub_tmp_dict["end"] = bed_record[2]
            sub_tmp_dict["strand"] = bed_record[5]
            tmp_dict[gene] = sub_tmp_dict
            i += 1

        if len(order_lst) >= 1:
            min_ind = min(order_lst) - int(expansion)
            max_ind = max(order_lst) + int(expansion) + 1
            new_gene_lst = gene_lst[min_ind:max_ind]

            j = 0
            for gene in new_gene_lst:
                j += 1
                if gene in gene_list:
                    tag = "HoGS"
                else:
                    tag = "NotHoG"
                strand = tmp_dict[gene]['strand']
                if strand == "+":
                    start = j*width + (j - 1)*length
                    end = j*width + j*length
                else:
                    end = j*width + (j - 1)*length
                    start = j*width + j*length

                print(tmp_dict[gene]['spec'],
                str(start),
                str(end),
                # tmp_dict[gene]['start'],
                # tmp_dict[gene]['end'],
                strand,
                str(length),# str(int(tmp_dict[gene]['end']) - int(tmp_dict[gene]['start'])),
                tag,
                "CDS",
                tmp_dict[gene]['spec']+str(j).zfill(4),
                gene)

with open(clufile) as f:
    spec = f.readline().strip().split("\t")
    clusters = f.readline().strip().split("\t")
    for ogs in spec_order_list:
        sub_spec = spec[ogs]
        gene_list = clusters[ogs].split(",")
        get_bed(sub_spec, gene_list, expansion)