#!/usr/bin/env python3
from operator import sub
import sys
import os


cluid = sys.argv[1]

datadir = "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/"
clu = datadir + "all_2021-10-15225207.clu"
size = datadir + "all_2021-10-15225207.size"

cmd = '(head -n 1 ' + clu + ';grep -w ' + cluid + ' ' + clu + ') > /dev/shm/tmp.clu'
os.system(cmd)

#spec_list = []
spec_order_list = [10,22,34,69,68,50,70,71,74,76] # range(77)
clu_dict = {}

with open('/dev/shm/tmp.clu') as f:

    def get_bed(spec,gene_list):
        data_path = "/data/user/chenym/data_fordatabase/rawdata/"
        bed = data_path + spec.split("_")[0] +".bed"
        tmp_dict = {}
        with open(bed) as bed_file:
            gene_bed = bed_file.readlines()
            i = 0
            for g in gene_bed:
                bed_record = g.strip().split("\t")
                gene = bed_record[3]
                if gene in gene_list:
                    i += 1
                    sub_tmp_dict ={}
                    sub_tmp_dict["chr"] = bed_record[0]
                    sub_tmp_dict["start"] = bed_record[1]
                    sub_tmp_dict["end"] = bed_record[2]
                    sub_tmp_dict["strand"] = bed_record[5]
                    tmp_dict[gene] = sub_tmp_dict
                    print("ODLS",spec,bed_record[1],bed_record[2],bed_record[5],"CDS",spec+str(i).zfill(3),
                    'null','null',"XXM",'NA','0',str(int(bed_record[2]) - int(bed_record[1])),"0.433333",gene,"LLL",spec+str(i).zfill(3))
        return(tmp_dict)

    spec = f.readline().strip().split("\t")
    clusters = f.readline().strip().split("\t")
    for ogs in spec_order_list:
        sub_spec = spec[ogs]
        #spec_list.append(sub_spec)
        gene_list = clusters[ogs].split(",")
        #print(sub_spec, gene_list)
        clu_dict[sub_spec] = gene_list
        dict_tmp = get_bed(sub_spec, gene_list)
        #print(ogs, "\n", dict_tmp ,"\n_____________________________________________")    
#print(clu_dict)



