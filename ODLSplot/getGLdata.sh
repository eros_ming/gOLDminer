#!/usr/bin/bash

cluid=$1
exp=$2
width=$3
length=$4

clu=/dev/shm/tmp.clu

python3 /data2/user2/xiexm/projs/GeneODL/ODLSplot/collinearity/get_links.py $cluid $exp $clu > "$cluid".links
python3 /data2/user2/xiexm/projs/GeneODL/ODLSplot/collinearity/get_genesV2.0.py $cluid $exp $clu $width $length > "$cluid".genes
