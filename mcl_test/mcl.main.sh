#!/usr/bin/bash

#SBATCH --job-name=mcl.Main
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=5gb

module load mcl/12-068  #加载工具

prefix=$1  #创建输出结果文件夹并设定输出文件前缀
tb=$2 #改成和01一样的基因组列表文件路径
work_path=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821  #工作目录，和01 work_path一样的的数据输出目录保持一致
#two_genomes_match_path=$work_path/pairwise_comp  #两两比较数据目录，和02 wp一样的的数据目录保持一致
two_genomes_match_path=/data2/user2/xiexm/projs/GeneODL/pipeline/00.PrepData/CollinearPairsDB_20220122
gene_backbone_path=$work_path/${prefix}_`date +%Y%m%d`  #第三步结果输出目录
out_prefix=${prefix}_`date +%Y-%m-%d%H%M%S`
clu2matrix=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/clu_to_matrix/00.clu2matrix.sh

mkdir -p $gene_backbone_path
mkdir -p /dev/shm/backbone
cd /dev/shm/backbone

for genome_from in `cat $tb | awk '{print $1"_"$2}' | tr '\n' ' '`;do
    for genome_to in `cat $tb | awk '{print $1"_"$2}' | tr '\n' ' '`;do
		file=$two_genomes_match_path/${genome_from}-${genome_to}.link
		if [ ! -x "$file" ]; then
			cat $file | awk '$5 != 0{print $1":"$2,$3":"$4,$5}' >> ${out_prefix}.tmp
		else
			echo $genome_from $genome_to >> ${out_prefix}.wo
		fi
	done
done

#mcl
mcxload -abc ${out_prefix}.tmp --stream-mirror -write-tab ${out_prefix}.tab -o ${out_prefix}.mci

mcl ${out_prefix}.mci -I 2 -o ${out_prefix}.I20

mcxdump -icl ${out_prefix}.I20 -tabr ${out_prefix}.tab -o $gene_backbone_path/gene_backbone_$out_prefix

#backbone clu2matrix
cd $gene_backbone_path
sh $clu2matrix  $gene_backbone_path/gene_backbone_$out_prefix $out_prefix

#backbone clu2ind
awk '{print NF"\t"$0}' $gene_backbone_path/gene_backbone_$out_prefix > $gene_backbone_path/gene_backbone_${out_prefix}_ind

#clean tmp files
#/usr/bin/rm -rf /dev/shm/backbone

