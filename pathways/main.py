#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File       :   get_knGene_Onode
@Time     :   2022/04/19 00:34:18
@Author  :   Xie Xiaoming
@Version  :   1.0
@Contact :   erosminer@icloud.com
@License :   (C)Copyright 2017-2021, All rights reserved
@Desc    :   None
'''

# here put the import lib
import sys

gene_file = "./for_loop3.txt"
order_file = "./order.txt"

order_dict = {}
with open(order_file) as of:
    data = of.readlines()
    for line in data:
        re = line.strip().split(" ")
        cluid = re[1]
        order_dict[cluid] = re[0]


with open(gene_file) as gf:
    data = gf.readlines()
    for line in data:
        re = line.strip().split(" ")
        cluid = re[0]
        geneid = re[1]
        name = re[2]
        order = order_dict[cluid]
        print(order,line.strip())