# cluster <- read_table("/data2/user2/xiexm/projs/GeneODL/ODLSplot/collinearity/Pina.clu",
# 											)

library(gggenomes)

# data(emale_seqs)

p1 <- gggenomes(emale_genes,
							 emale_seqs ) + 
	geom_seq() + 
	geom_bin_label() +
	geom_gene(aes(fill=name)) +
	geom_gene_tag(aes(label=name), 
								nudge_y=0.1, 
								check_overlap = TRUE) +
	scale_fill_brewer("Genes", 
										palette="Dark2", 
										na.value="cornsilk3")

p1

#emale_links <- read_paf("/data2/user2/xiexm/projs/GeneODL/ODLSplot/collinearity/gggenomes/data-raw/emales/emales.paf")

p2 <- gggenomes(emale_genes,
							 # emale_links,
							 emale_seqs,
							 emale_links) + 
	geom_seq() + 
	geom_bin_label() +
	geom_gene(aes(fill=name)) +
	#geom_link() +
	geom_gene_tag(aes(label=name), 
								nudge_y=0.1, 
								check_overlap = TRUE) +
	scale_fill_brewer("Genes", 
										palette="Dark2", 
										na.value="cornsilk3")

p2


pina.genes <- read_table("/data2/user2/xiexm/projs/GeneODL/ODLSplot/collinearity/c_8200.genes",col_names = F)
colnames(pina.genes) <- c("seq_id","start","end","strand","width","source","type","feature_id","name")
#pina.genes$introns <- list(NULL)
#colnames(pina.genes)
# pina.seqs <- read_table("/data2/user2/xiexm/projs/GeneODL/ODLSplot/collinearity/Pina.seqs",col_names = F)
# colnames(pina.seqs) <- colnames(emale_seqs)
# 
# p3 <- gggenomes(pina.genes,
# 								pina.seqs) + 
# 	geom_seq() + 
# 	geom_bin_label() +
# 	geom_gene(aes(fill = name),size = 5) +
# 	geom_gene_tag(aes(labe = name), 
# 								nudge_y = 0.1, 
# 								check_overlap = TRUE) +
# 	scale_fill_brewer("Genes", 
# 										palette = "Dark2", 
# 										na.value = "cornsilk3")
# 
# p3
# pina.genes$start <- round(pina.genes$start/100)
# pina.genes$end <- round(pina.genes$end/100)
p3 <- gggenomes(genes = pina.genes) +
	geom_seq() +
	geom_gene(aes(fill = source),
						size = 2,
						inherit.aes = F) + 
	geom_gene_tag(aes(label = name), 
								nudge_y = 0.1, 
								check_overlap = TRUE) +
	theme(axis.line.x = element_blank(),
				axis.ticks = element_blank(),
				axis.text.x = element_blank(),
				legend.position = "none") +
	scale_fill_brewer("Genes", 
										palette = "Dark2", 
										na.value = "cornsilk3")

p3
# pina.genes$width
# pina.seqs$length[c(3,4,6,7)] <- round(pina.seqs$length[c(3,4,6,7)]/10)
# 
# table(emale_genes$introns)
link <- read_table("/data2/user2/xiexm/projs/GeneODL/ODLSplot/collinearity/c_8200.links",col_names = F)
colnames(link) <- c("name","name2","seq_id","seq_id2")
# part_link1 <- dplyr::filter(pina.genes,name %in% link$seq_id)
# part_link2 <- dplyr::filter(pina.genes,name %in% link$seq_id2)
tbl <- pina.genes[c("name","start","end","strand")]
part_link1 <- dplyr::left_join(link, tbl)

tbl_rev <- pina.genes[c("name","start","end","source")]
colnames(tbl_rev) <- c("name2","start","end","source")
part_link2 <- dplyr::left_join(link, tbl_rev)

pina.links <- dplyr::left_join(part_link1, 
															 part_link2,
															 by = "name")
pina.links$length <- pina.links$end.x - pina.links$start.x
pina.links$length2 <- pina.links$end.y - pina.links$start.y
pina_links <- pina.links[c(3,14,5:7,10,15,11:12,13)]
colnames(pina_links) <- c(colnames(part_links),"td")

pina.genes$seq_id <- factor(pina.genes$seq_id,
														levels = unique(pina.genes$seq_id))

p4 <- gggenomes(genes = pina.genes,links = pina_links) +
	geom_seq() +
	geom_bin_label() +
	geom_link(aes(color = td), offset = 0.05) +
	geom_gene(aes(fill = source,color = source),
						size = 2,
						shape = 0) + 
	# geom_gene_tag(aes(label = name), 
	# 							nudge_y = 0.1, 
	# 							check_overlap = TRUE) +
	theme(axis.line.x = element_blank(),
				axis.ticks = element_blank(),
				axis.text.x = element_blank(),
				legend.position = "none")

p4

c_14188.genes <- read_table("/data2/user2/xiexm/projs/GeneODL/ODLSplot/collinearity/all_76_c_14188.genes",col_names = F)
colnames(c_14188.genes) <- c("seq_id","start","end","strand","width","source","type","feature_id","name")

p5 <- gggenomes(genes = c_14188.genes) +
	geom_seq() +
	geom_gene(aes(fill = source),
						size = 2,
						inherit.aes = F) + 
	geom_gene_tag(aes(label = name), 
								nudge_y = 0.1, 
								check_overlap = TRUE) +
	theme(axis.line.x = element_blank(),
				axis.ticks = element_blank(),
				axis.text.x = element_blank(),
				legend.position = "none") +
	scale_fill_brewer("Genes", 
										palette = "Dark2", 
										na.value = "cornsilk3")

p5
