## *12.14 ggtree----

hc <- hclust(dist(t(data_all_size[,1:76])))
plot(hc)
tree <- treeio::as.phylo(hc)
library(ggtree)

tree <- groupClade(tree, .node = c(80,83,85,87,89,93,92))
ggtree(tree, aes(color = group, linetype = group)) + geom_text2(aes(label = node))

## *p1 ----
p1 <- ggtree(tree, layout = "circular", size = 2, 
						 aes(color = group)) + 
	geom_tippoint(size = 3, hjust = -.2) + 
	#geom_text2(aes(subset =! isTip, label = node), hjust = -.3) + 
	geom_tiplab2(aes(angle = angle), hjust = -0.1) +
	geom_strip(27, 33, 
						 barsize = 3, 
						 color = "#ff61cc", 
						 offset = 140, 
						 label = "Subgenome D", 
						 offset.text = 20, 
						 angle = 230, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(8, 9, 
						 barsize = 3, 
						 color = "#c77cff", 
						 offset = 140, 
						 label = "Subgenome A", 
						 offset.text = 20, 
						 angle = 160, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(68, 67, 
						 barsize = 3, 
						 color = "#00a9ff", 
						 offset = 140, 
						 label = "Rye", 
						 offset.text = 20, 
						 angle = 115, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(19, 21, 
						 barsize = 3, 
						 color = "#00bfc4", 
						 offset = 140, 
						 label = "Subgenome B", 
						 offset.text = 20, 
						 angle = 75, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(65, 50, 
						 barsize = 3, 
						 color = "#00be67", 
						 offset = 140, 
						 label = "Barley", 
						 offset.text = 20, 
						 angle = 0, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(76, 75, 
						 barsize = 3, 
						 color = "#82b20b", 
						 offset = 140, 
						 label = "Maize", 
						 offset.text = 20, 
						 angle = 290, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(74, 73, 
						 barsize = 3, 
						 color = "#d09d12", 
						 offset = 140, 
						 label = "Rice", 
						 offset.text = 20, 
						 angle = 280, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) 

p2 <- ggtree::rotate(p1,88)
p3 <- ggtree::rotate(p2,86)

## *p4 ----
p4 <- ggtree(tree, size = 2, 
						 aes(color = group)) + 
	geom_tippoint(size = 3, hjust = -.2) + 
	geom_tiplab(hjust = -0.1) +
	geom_strip(27, 33, 
						 barsize = 3, 
						 color = "#ff61cc", 
						 offset = 140, 
						 label = "Subgenome D", 
						 offset.text = 20, 
						 angle = 270, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(8, 9, 
						 barsize = 3, 
						 color = "#c77cff", 
						 offset = 140, 
						 label = "Subgenome A", 
						 offset.text = 20, 
						 angle = 270, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(68, 67, 
						 barsize = 3, 
						 color = "#00a9ff", 
						 offset = 140, 
						 label = "Rye", 
						 offset.text = 20, 
						 angle = 270, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(19, 21, 
						 barsize = 3, 
						 color = "#00bfc4", 
						 offset = 140, 
						 label = "Subgenome B", 
						 offset.text = 20, 
						 angle = 270, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(65, 50, 
						 barsize = 3, 
						 color = "#00be67", 
						 offset = 140, 
						 label = "Barley", 
						 offset.text = 20, 
						 angle = 270, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(76, 75, 
						 barsize = 3, 
						 color = "#82b20b", 
						 offset = 140, 
						 label = "Maize", 
						 offset.text = 20, 
						 angle = 270, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(74, 73, 
						 barsize = 3, 
						 color = "#d09d12", 
						 offset = 140, 
						 label = "Rice", 
						 offset.text = 20, 
						 angle = 270, 
						 fontsize = 6, 
						 hjust = "center", 
						 extend = 0.3) 

p5 <- ggtree::rotate(p4,88)
p6 <- ggtree::rotate(p5,86)


## *p7 ----
p7 <- ggtree(tree, size = 1.2) + 
	geom_tippoint(size = 3, hjust = -.2) + 
	#geom_tiplab(hjust = -0.1) +
	geom_strip(27, 33, 
						 barsize = 3, 
						 color = "#ff61cc", 
						 offset = 10, 
						 label = "Subgenome D", 
						 offset.text = 10, 
						 angle = 270, 
						 fontsize = 4, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(8, 9, 
						 barsize = 3, 
						 color = "#c77cff", 
						 offset = 10, 
						 label = "Subgenome A", 
						 offset.text = 10, 
						 angle = 270, 
						 fontsize = 4, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(68, 67, 
						 barsize = 3, 
						 color = "#00a9ff", 
						 offset = 10, 
						 label = "Rye", 
						 offset.text = 10, 
						 #angle = 270, 
						 fontsize = 4, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(19, 21, 
						 barsize = 3, 
						 color = "#00bfc4", 
						 offset = 10, 
						 label = "Subgenome B", 
						 offset.text = 10, 
						 angle = 270, 
						 fontsize = 4, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(65, 50, 
						 barsize = 3, 
						 color = "#00be67", 
						 offset = 10, 
						 label = "Barley", 
						 offset.text = 10, 
						 angle = 270, 
						 fontsize = 4, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(76, 75, 
						 barsize = 3, 
						 color = "#82b20b", 
						 offset = 10, 
						 label = "Maize", 
						 offset.text = 10, 
						 #angle = 270, 
						 fontsize = 4, 
						 hjust = "center", 
						 extend = 0.3) +
	geom_strip(74, 73, 
						 barsize = 3, 
						 color = "#d09d12", 
						 offset = 10, 
						 label = "Rice", 
						 offset.text = 10, 
						 #angle = 270, 
						 fontsize = 4, 
						 hjust = "center", 
						 extend = 0.3)
p5 <- ggtree::rotate(p7,88)
p6 <- ggtree::rotate(p5,86)
p7 <- ggtree::rotate(p6,91)

library(ggstance)

p <- ggtree(tree,size = 1.2) + geom_tiplab()
d1 <- data.frame(id = tree$tip.label)
p1 <- p7 %<+% d1 + geom_tippoint(aes(color = group))
d2 <- data.frame(id = tree$tip.label, val = data$percent)
p2 <- facet_plot(p1, panel = "dot", data = d2, geom = geom_point, 
								 aes(x = val), color = 'firebrick') + theme_tree2()

p3 <- facet_plot(p1, panel = "Tandem Gene Percent", data = d2, geom = geom_barh, 
								 aes(x = val, fill = group), stat = 'identity') + theme_tree2()
d3 <- data.frame(id = tree$tip.label, val = data$tandem)
p4 <- facet_plot(p3, panel = "Tandem Gene Number", data = d3, geom = geom_barh, 
								 aes(x = val, fill = group), stat = 'identity') + theme_tree2()
d4 <- data.frame(id = tree$tip.label, val = data$total)
p5 <- facet_plot(p4, panel = "Total Gene Number", data = d4, geom = geom_barh, 
								 aes(x = val, fill = group), stat = 'identity') + theme_tree2()


#genomes <- unlist(strsplit(tree$tip.label,"_"))[seq(1,152,2)]
p5