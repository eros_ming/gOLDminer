TreePlotByODLSnode3x <- function(data) {
	suppressMessages(library(ggtree))
	suppressMessages(library(ggstar2))
  
  options(warn = -1)
  gg <- ggtree(ape::read.tree("~/Desktop/3x_tree_origin.nwk"))
  tree_df <- gg$data

  # data <- data_size_max["c_18850",]
  HoC_Size <- as.numeric(data[1:10])
  df <- data.frame(HoC_Size)
  rownames(df) <- rownames(read.table("/data2/user2/xiexm/Desktop/may_tax.txt",
    row.names = 1, header = TRUE
  ))


  odls_tag <- unlist(strsplit(as.character(data[15]), "|", fixed = T))
  tag_l <- strsplit(odls_tag, ":")

  temp_df <- data.frame(
    odls = c(), node = c(),
    shape = c(), size = c(),
    color = c(),
    x = c(), y = c()
  )
  
  for (i in 1:4) {
    # i = 3
    # cat(i,"\n")
    tag <- tag_l[[i]]
    if (length(tag) != 1 & tag[2] != "NA") {
      node_l <- as.numeric(unlist(strsplit(tag[2], ",")))
      # shape <- c(1, 0, 4, 6)[i]
      shape <- c(21, 22, 4, 3)[i]
      size <- c(10, 5, 5, .2)[i]

      for (node in node_l) {
        # node = 7
        node <- ifelse(node >= 20, 10, node)
        node <- ifelse(node == 19, 9, node)
				#cat(node," | ",as.character(data[14]),"\n")
        x <- tree_df$x[tree_df$node == node]
        y <- tree_df$y[tree_df$node == node]
        
        col <- ifelse(node > 10,"grey","gray")
        cut <- ifelse(node > 10,.5,.175)
        
        tmp_df <- data.frame(
          odls = c(tag[1]),
          node = c(node),
          shape = c(shape),
          size = c(size),
          color = c(col),
          x = c(x - cut),
          y = c(y)
        )
        
        temp_df <- rbind(temp_df, tmp_df)
      }
    }
  }
  
  off.set <- ifelse(temp_df$node[temp_df$odls == "O"] > 10,.1,.3)
	
  legend_df <- data.frame(
    shape = c(21, 22, 4, 3),
    color = c(rep("grey", 4)),
    size = c(8,8,8,.01),
    x = c(rep(-0.04, 4)),
    y = c(9, 8, 7, 6) + 1,
    note = c(
      "Origin", "Duplicate", "Loss",
      "Shrink"
    )
  ) # lineage-specific
	
  shrink_df <- dplyr::filter(temp_df,odls == "S")
  no_shrink_df <- dplyr::filter(temp_df,odls != "S")
  
  # library(ggimage)
  # img <- "/data2/user2/xiexm/projs/GeneODL/grey.png"
  
  p <- gg +
  	geom_rootedge(.5) +
    geom_tiplab(align = TRUE, linetype = "solid", fontface = "italic", offset = off.set) +
    geom_point(
      data = temp_df, aes(x, y),
      shape = temp_df$shape,
      fill = temp_df$color, size = temp_df$size, stroke = 1
    ) +
  	geom_star(data = shrink_df, 
  						aes(x, y),
  						size = 3,
  						color = "black",
  						fill = "grey",
  						starshape = 31) +
  	# geom_image(image = img, data = shrink_df, aes(x, y), size = .06) +
    geom_point(
      data = legend_df, aes(x, y),
      shape = legend_df$shape,
      fill = legend_df$color, size = legend_df$size, stroke = 1
    ) +
  	# geom_image(image = img,aes(-0.04, 7), size = .06) +
  	geom_star(aes(-0.04, 7), 
  						size = 7,
  						color = "black",
  						fill = "grey",
  						starshape = 31) +
    geom_text(
      data = legend_df,
      aes(x + 0.5, y, label = note),
      fontface = "bold", hjust = 0
    ) #+ geom_text(aes(-0.04,10, label = data[14]), fontface = "bold", hjust = 0)

  ##* ggplot2 画热图####
  suppressMessages(library(ggplot2))
  suppressMessages(library(RColorBrewer))
  
  df2 <- df
  df2$Species <- rownames(df2)
  df2$Species <- factor(df2$Species, levels = rev(rownames(df2)))
  max_size <- max(df2$HoC_Size)
  if (max_size < 11) {
    color_list <- colorRampPalette(brewer.pal(9, "YlOrRd"))(10)
  } else {
    color_list <- colorRampPalette(brewer.pal(9, "YlOrRd"))(max_size)
  }
  transColor <- function(data) {
    size <- as.numeric(data[1])
    color <- ifelse(size == 0, "grey", color_list[size])
    return(color)
  }
  df2$Type <- apply(df2, 1, transColor)
  df2$Xvar <- "HoC_Size"

  gene_name <- ifelse(data[16] != "",data[16],"")
  
  p2 <- ggplot(df2, aes(Xvar, Species)) +
    geom_tile(fill = df2$Type, colour = "black") +
    geom_text(aes(label = HoC_Size), col = "black", size = 5) +
    theme_bw() +
    xlab(data[14]) +
  	ylab(gene_name) +
  	ggtitle("") +
  	scale_y_discrete(position = "right") + 
  	theme(
  		#axis.title.y = element_text(color = "grey"),
  		axis.title.y.right = element_text(color = "blue",face = "italic"),
  		axis.text = element_blank(),
  		axis.ticks = element_blank(),
  		panel.border = element_blank(),
  		panel.grid.major = element_blank(),
  		panel.grid.minor = element_blank(),
  		legend.position = "none")

  ## 拼接图片 ####
  suppressMessages(library(ggpubr))
  p3 <- p + xlim(-1, 12.5) #+ theme_tree2()
  p4 <- ggarrange(p3, p2, widths = c(5, 1), align = "hv")

  # pdf(file = paste("/data2/user2/xiexm/Desktop/knGeneV2/",
  # 								 data[12], "-", data[13], "-", data[14], ".pdf",
  # 								 sep = ""
  # ), height = 4, width = 7)
  # print(p4)
  # dev.off()
  
  p4
}

load("~/Desktop/2022-03-03-all-ODLS.RData", envir = parent.frame(), verbose = FALSE)

data_size_max$gene_name <- ""
TreePlotByODLSnode3x(data_size_max["c_824", ])

knGene <- read.table("/data2/user2/xiexm/projs/GeneODL/data/knowGenes_merge.txt",
										 header = F, col.names = c("cluid","gene_name"))

data_size_knG <- data_size_max[rownames(data_size_max) %in% knGene$cluid,]
data_size_knG$odls_tag <- apply(data_size_knG, 1, FindODLS)
for (i in 1:nrow(data_size_knG)) {
	data_size_knG$gene_name[i] <- as.character(knGene$gene_name[knGene$cluid == rownames(data_size_knG)[i]])
}

apply(data_size_knG, 1, TreePlotByODLSnode3x)

data_ABD_o <- dplyr::filter(data_size_max,Order > 17)
write.table(rownames(data_ABD_o),"~/projs/GeneODL/data/abd.rownames",
						quote = F,col.names = F,row.names = F)




paste0(levels(h$group)[c(10,4,2,1,3,5,6,9,8,7)],collapse = ",")


library(treedataverse)
