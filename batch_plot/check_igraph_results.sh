#!/usr/bin/bash
for gf in TaGRAS TaSPL MADS-box;do
	 while read line;do
		grep $line $HOME/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/batch_plot/result_nocontrol/IWGSCv1p1.nocontrol.cludb | cut -d' ' -f 5 >> ${gf}.igraph.QUERY
	 done < $gf
done
