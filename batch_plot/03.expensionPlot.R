library(tidyverse)

size_file <- "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/statistics/all.td.size"
data_size <- read.table(size_file, header = T, row.names = 1)

## 保存环境变量 ----
# Save my workspace to complete_image.RData in the data folder of my working directory
save.image(file = "~/R/Rdata/fun_analysis_image_12_08.RData")
load(file = "~/R/Rdata/fun_analysis_image_12_08.RData")


## 数据处理分组####

data_size$Triticeae <- apply(data_size[, c(1:68)], 1, mean, na.rm = TRUE)
data_size$Traes <- apply(data_size[, c(1:46)], 1, mean, na.rm = TRUE)
data_size$Poaceae <- apply(data_size[, c(69:76)], 1, mean, na.rm = TRUE)
data_size$TraesA <- apply(data_size[, c(1:12, 37:38, 46)], 1, mean, na.rm = TRUE)
data_size$TraesB <- apply(data_size[, c(13:24, 39:40)], 1, mean, na.rm = TRUE)
data_size$TraesD <- apply(data_size[, c(25:36, 41:45)], 1, mean, na.rm = TRUE)
data_size$Osat <- apply(data_size[, c(72:74)], 1, mean, na.rm = TRUE)
data_size$Zmay <- apply(data_size[, c(75:76)], 1, mean, na.rm = TRUE)
data_size$Hvul <- apply(data_size[, c(47:66)], 1, mean, na.rm = TRUE)
data_size$Scer <- apply(data_size[, c(67:68)], 1, mean, na.rm = TRUE)
data_size$Aetau <- apply(data_size[, c(41:45)], 1, mean, na.rm = TRUE)

data <- data_size[, c("Triticeae", "Poaceae")]
data <- ceiling(data)
data$slope <- data[, 1] / data[, 2]

data <- data_size[, c("TraesA", "TraesB")]
data <- data_size[, c("TraesB", "TraesD")]
data <- data_size[, c("TraesA", "TraesD")]
data <- data_size[, c("Traes", "Hvul")]

## 方差检验####
tag <- "P-expansion"
data_T_expansion <- dplyr::filter(data, slope <= 0.5) # FIXME: 这里反了
write.table(data_size[rownames(data_P_expansion), ],
  file = paste0("/data2/user2/xiexm/", tag, ".size"),
  quote = F, sep = "\t"
)
data_for_test <- data_size[c(rownames(data_T_expansion)), ]

data_P_expansion <- dplyr::filter(data, slope >= 2)
write.table(data_size[rownames(data_P_expansion), ],
  file = paste0("/data2/user2/xiexm/", tag, ".size"),
  quote = F, sep = "\t"
)
data_for_test <- data_size[c(rownames(data_P_expansion)), ]

data_T_nom_expansion <- dplyr::filter(data, slope < 1, slope > 0.5)
data_for_test <- data_size[c(rownames(data_T_nom_expansion)), ]

data_P_nom_expansion <- dplyr::filter(data, slope < 2, slope > 1)
data_for_test <- data_size[c(rownames(data_P_nom_expansion)), ]

data_no_expansion <- dplyr::filter(data, slope == 1)
data_for_test <- data_size[c(rownames(data_no_expansion)), ]


job::job(title = "no-expansion", {
  test <- function(i) {
    tag <- "no-expansion"
    Tri <- as.numeric(data_for_test[i, c(1:68)])
    Poa <- as.numeric(data_for_test[i, c(69:76)])
    # ftest <- var.test(as.numeric(data_size[1, c(1:68)]),as.numeric(data_size[1, c(69:76)]))
    # ttest <- t.test(as.numeric(data_size[1, c(1:68)]),as.numeric(data_size[1, c(69:76)]), var.equal = T)
    Ftest <- var.test(Tri, Poa)
    # data_for_test$p[i] <- Ftest$p.value
    if (Ftest$p.value > 0.05) { # 方差齐
      Ttest <- t.test(Tri, Poa, var.equal = T)
    } else { # 方差不齐
      Ttest <- t.test(Tri, Poa, var.equal = F)
    }
    # data_for_test$p2[i] <- Ttest$p.value
    x <- paste(rownames(data_for_test)[i], Ftest$p.value, Ttest$p.value, tag, sep = "\t")
    write.table(x, file = "~/tmp/Test.results", append = T, quote = F, row.names = F, col.names = F)
  }
  ## R 并行
  library(parallel)
  l <- length(data_for_test[, 1])
  x <- 1:l
  if (l >= 14) {
    cl <- makeCluster(14)
  } else {
    cl <- makeCluster(l)
  }
  clusterExport(cl, c("data_for_test"), environment())
  parSapply(cl, x, test)
  stopCluster(cl)
})
## 百分比堆积图####
data_test_result <- read.table("~/tmp/Test.results", col.names = c("cluid", "Fp", "Tp", "Tag"))
table(data_test_result$Tag)
data_test_result$test <- ifelse(data_test_result$Tp > 0.05, "equal", "not equal")
data_test_result$Tag <- factor(data_test_result$Tag,
  levels = unique(data_test_result$Tag)[c(1, 3, 2, 4, 5)],
  labels = unique(data_test_result$Tag)[c(2, 4, 1, 3, 5)]
)

# ggplot(data_test_result, aes(Tag, fill = test)) +
#   geom_bar(stat = "count", position = "fill") +
#   # ggtitle("The Financial Performance of Five Giant")+
#   # theme_economist(base_size=14)+
#   scale_fill_manual(values = c("#abc46e", "#da7b7a")) +
#   ggprism::theme_prism() +
#   theme( # axis.ticks.length = unit(0.5, "cm"),
#     legend.position = "bottom"
#   ) +
#   guides(fill = guide_legend(title = NULL)) +
#   scale_y_continuous(position = "right") +
#   ylab("") +
#   xlab("") +
#   coord_flip()


data_for_bar <- data_test_result %>%
  group_by(Tag) %>%
  count(test)

gg <- ggplot(data_for_bar, aes(Tag, n, fill = test)) +
  geom_bar(stat = "identity", position = "fill") +
  # ggtitle("The Financial Performance of Five Giant")+
  # theme_economist(base_size=14)+
  scale_fill_manual(values = c("#abc46e", "#da7b7a")) +
  ggprism::theme_prism() +
  theme( # axis.ticks.length = unit(0.5, "cm"),
    legend.position = "bottom"
  ) +
  guides(fill = guide_legend(title = NULL)) +
  scale_y_continuous(position = "right") +
  ylab("") +
  xlab("") +
  coord_flip()

# p + geom_text(x = c(, 1:5), y = ylist, aes(label = as.character(n)), , position = "identity")
# geom_text(aes(label = as.character(n)), position = position_stack(vjust = 0.5))
# ylist <- c()
P <- gg
for (i in 1:5) {
  p1 <- data_for_bar$n[2 * i - 1] / (data_for_bar$n[2 * i - 1] + data_for_bar$n[2 * i])

  p <- (1 - p1) + p1 / 2
  # ylist <- append(ylist,c(p1/2,p))
  P <- P + annotate("text",
    x = i, y = p, label = as.character(data_for_bar$n[2 * i - 1]),
    color = "black", size = 5, fontface = "bold"
  )
  P <- P + annotate("text",
    x = i, y = (1 - p1) / 2, label = as.character(data_for_bar$n[2 * i]),
    color = "black", size = 5, fontface = "bold"
  )
}
P
## Expansion Plot####


limmit <- 50
data[data > limmit] <- limmit + 1
#* ~ TODO 比值换差值 ----
ggplot(data, aes(x = TraesB, y = TraesD)) +
  geom_bin2d(bins = 50) +
  # geom_abline(intercept = 0, slope = 2, color = "grey", linetype = "dashed") +
  geom_abline(intercept = 5, slope = 1, color = "grey", linetype = "dashed") +
  geom_abline(intercept = -5, slope = 1, color = "grey", linetype = "dashed") +
  geom_abline(intercept = 0, slope = 1, color = "grey") +
  # geom_abline(intercept = 0, slope = 0.5, color = "grey", linetype = "dashed") +
  geom_hline(aes(yintercept = limmit), color = "black") +
  geom_vline(aes(xintercept = limmit), color = "black") +
  scale_x_continuous(expand = c(0, 0)) +
  scale_y_continuous(expand = c(0, 0)) +
  ggprism::theme_prism() +
  # 设置图例名称
  scale_fill_stepsn(
    colours = cols,
    breaks = breaks,
    labels = lables,
    name = "Number of gene clusters\nper position"
  ) +
  xlab(paste0("Gene Cluster Average Size in ", colnames(data[1]), " Group")) +
  ylab(paste0("Gene Cluster Average Size in ", colnames(data[2]), " Group"))
# annotate("text", x = limmit - 3, y = limmit - 3, label = paste0(limmit, "+")) +
# annotate("text", x = 40, y = 10, label = "T-expansion") +
# annotate("text", x = 30, y = 20, label = "T-nom-expansion") +
# annotate("text", x = 10, y = 40, label = "P-expansion") +
# annotate("text", x = 20, y = 30, label = "p-nom-expansion") +
# geom_curve(
#   aes(x = x1, y = y1, xend = x2, yend = y2),
#   data = df,
#   arrow = arrow(length = unit(0.03, "npc"))
# )
# theme(panel.background = element_rect(colour = "black", size=1))
df <- data.frame(x1 = limmit + 1, y1 = limmit + 1, x2 = limmit - 3, y2 = limmit - 2)
cols <- c(
  c("#ff0000", "#fdd017", "#ffff00", "#a0c544", "#95b9c7", "#6698ff", "#6698ff"),
  # rev(c("#ff0000", "#fdd017", "#ffff00", "#a0c544", "#95b9c7", "#6698ff", "#6698ff")),
  rep("#005ae2", 1000)
)
breaks <- c(2, 8, 25, 75)
lables <- c("2+", "8+", "25+", "75+")

#* pie ####
statistic_file <- "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/statistics/num.statistic"
data_for_pie <- read.table(statistic_file, col.names = c("num", "type"))
data_for_pie$tag <- c("01", "02", "11", "12")

myLabel <- as.vector(data_for_pie$tag) ## 转成向量，否则图例的标签可能与实际顺序不一致
myLabel <- paste(myLabel, "(", round(data_for_pie$num / sum(data_for_pie$num) * 100, 2), "%)", sep = "") ## 用 round() 对结果保留两位小数

p <- ggplot(data_for_pie, aes(x = "", y = num, fill = tag)) +
  geom_bar(stat = "identity", width = 1) +
  coord_polar(theta = "y") +
  labs(x = "", y = "", title = "") +
  ggprism::theme_prism() +
  theme(axis.ticks = element_blank()) +
  theme(legend.title = element_blank(), legend.position = "right") +
  scale_fill_discrete(breaks = data_for_pie$tag, labels = myLabel, ) + ## 将原来的图例标签换成现在的myLabel
  scale_fill_manual() +
  theme(axis.text.x = element_blank()) ## 白色的外框即是原柱状图的X轴，把X轴的刻度文字去掉即可
p

library(ggpubr)
p3 <- ggpie(data_for_pie, "num",
  label = myLabel, # 设置标签
  lab.pos = "out", lab.font = "white", # 放置标签到饼图扇形区域，并设置标签颜色
  fill = "group", color = "white",
  palette = get_palette("Set1", 20)
)
p3

#* igraph plot####
#### 三元图####


data <- data_size[, c("TraesA", "TraesB", "TraesD")]
# limmit <- 50
# data[data > limmit] <- limmit + 1
library(vcd)

smM <- as.matrix(data)
color <- apply(smM, 1, function(t) colnames(smM)[which.max(t)])
color[color == "TraesA"] <- "#009831"
color[color == "TraesB"] <- "#911d81"
color[color == "TraesD"] <- "#f19700"

smM <- smM + 0.000000000000000000001 # 为何重要？

ternaryplot(smM,
  prop_size = TRUE,
  grid_color = "black",
  labels_color = "black",
  main = "",
  labels = "outside",
  col = color
)

grid_legend(0.85, 0.7, 19, c("#009831", "#911d81", "#f19700"), colnames(data), frame = FALSE, size = 1)

library(ggtern)
# Or you can apply a color gradient to space between the contour lines
ggtern(data, aes(TraesA, TraesB, TraesD)) + # define data sources
  stat_density_tern(aes(fill = ..level.., alpha = ..level..)) + # now you need to use stat_density_tern
  scale_fill_gradient2(high = "red") + # define the fill color
  guides(color = "none", fill = "none", alpha = "none") + # we don't want to display legend items
  # theme_bvbg()
  # theme_bvbw()
  # theme_bluelight()
  theme_tropical()
# theme_matrix()
# theme_custom()
# theme_void()
# theme_rgbg()
# theme_bw()
# theme_minimal()
# theme_rgbw()
# theme_light()
# theme_dark()

#### 三维散点图####
library(plot3D)
# scatter3D(x, y, z, ..., colvar = z, phi = 40, theta = 40,
#            col = NULL, breaks = NULL,
#            colkey = NULL, panel.first = NULL,
#            clim = NULL, clab = NULL,
#            bty = "b", CI = NULL, surf = NULL,
#            add = FALSE, plot = TRUE)
data$type <- color
with(data, scatter3D(
  x = TraesA, y = TraesB, z = TraesD,
  pch = 21, cex = 1.5, col = "black", bg = data$type,
  xlab = " ",
  ylab = " ",
  zlab = " ",
  ticktype = "detailed", bty = "f", box = TRUE,
  theta = 360, phi = 15, d = 3,
  colkey = FALSE
))
legend("right",
  title = "", legend = c("TraesA", "TraesB", "TraesD"), pch = 21,
  cex = 1, y.intersp = 1, pt.bg = c("#009831", "#911d81", "#f19700"), bg = "white", bty = "n"
)


max(dataS$TraesA)
data["c_1304", ]$type <- "#e7212f"
data["c_24972", ]$type <- "#e7212f"
data["c_2332", ]$type <- "#e7212f"
#### 热图####

dataT <- dplyr::filter(data_size, Triticeae >= 4, Poaceae <= 2)
dataA <- dplyr::filter(data, TraesD <= 2, TraesB <= 2, TraesA >= 4)
dataB <- dplyr::filter(data, TraesD <= 2, TraesB >= 4, TraesA <= 2)
dataD <- dplyr::filter(data, TraesD >= 4, TraesB <= 2, TraesA <= 2)
dataH <- dplyr::filter(data_size, TraesD <= 2, TraesB <= 2, TraesA <= 2, Hvul >= 4)


# heatmap_data <- data_size[c(rownames(dataH)), 1:76]
heatmap_data <- data_size[clu3, 1:76]
geneFam_t <- t(heatmap_data)

Group <- c(
  rep("01.He_A", 12), rep("02.He_B", 12), rep("03.He_D", 12), "04.Te_A", "04.Te_A", "05.Te_B", "05.Te_B",
  rep("06.Di_D", 5), "07.Di_A", rep("08.Barley_H", 20), rep("09.Rye_S", 2), "10.Te", rep("11.Other", 7)
)
group_sample <- data.frame(Group)

rownames(group_sample) <- rownames(geneFam_t)
pheatmap::pheatmap(geneFam_t[, gtools::mixedsort(colnames(geneFam_t))],
  cluster_rows = F,
  # cluster_cols = F,
  # display_numbers = T,
  border = F,
  # show_rownames = F,
  # show_colnames = F,
  annotation_row = group_sample,
  color = c("white", "grey", "#99D8C9", "#238B45", "#00441B"),
  legend_breaks = c(0, 2, 4, 10, max(geneFam_t)),
  breaks = c(-1, 0, 2, 4, 10, max(geneFam_t))
)

#### GO 分析####
# clu_file <- "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/all_2021-10-15225207.clu"
# data_clu <- read.table(clu_file, header = T, row.names = 1)
# data_clu_td <- data_clu[c(rownames(data_size)),]
# write.table(data_clu_td,"/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/statistics/all.td.clu",
#             quote = F,sep = '\t')

clu_file <- "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/statistics/all.td.clu"
data_clu <- read.table(clu_file, header = T, row.names = 1)

go_data <- data_clu[c(rownames(dataT)), "IWGSCv1p1_chrND"]
# c("IWGSCv1p1_chrND","IWGSCv1p1_chrNA","IWGSCv1p1_chrNB")]
gene_list <- unlist(strsplit(as.character(go_data), ","))
write.table(gene_list, "../Td-expansion_gene.list", quote = F, col.names = F, row.names = F)


# 谱系特有基因(Lineage-specific genes，LSGs)####
# A,B,D,H,S,N,N
library(tidyverse)

all_size_file <- "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/all_2021-10-15225207.size"
data_all_size <- read.table(all_size_file, header = T, row.names = 1)

colnames(data_all_size)
data_all <- data_all_size

data_all$Traes.He.A <- apply(data_all[, c(1:12)], 1, mean, na.rm = TRUE)
data_all$Traes.He.B <- apply(data_all[, c(13:24)], 1, mean, na.rm = TRUE)
data_all$Traes.He.D <- apply(data_all[, c(25:36)], 1, mean, na.rm = TRUE)
data_all$Traes.Te.A <- apply(data_all[, c(37:38, 46)], 1, mean, na.rm = TRUE)
data_all$Traes.Te.B <- apply(data_all[, c(39:40)], 1, mean, na.rm = TRUE)
data_all$Traes.Di.A <- data_all[, 46]
data_all$Traes.Di.D <- apply(data_all[, c(41:45)], 1, mean, na.rm = TRUE)
data_all$Traes <- apply(data_all[, c(1:46)], 1, mean, na.rm = TRUE)
data_all$Hvul <- apply(data_all[, c(47:66)], 1, mean, na.rm = TRUE)
data_all$Scer <- apply(data_all[, c(67:68)], 1, mean, na.rm = TRUE)
data_all$Telo <- data_all[, 69]
data_all$Astr <- data_all[, 70]
data_all$Bdis <- data_all[, 71]
data_all$Osat <- apply(data_all[, c(72:74)], 1, mean, na.rm = TRUE)
data_all$Zmay <- apply(data_all[, c(75:76)], 1, mean, na.rm = TRUE)

data_sepc <- ceiling(data_all[, -c(1:76)])
rm(data_all, data_all_size)

data_sepc$sum <- apply(data_sepc[, c(-8, -16)], 1, sum)

write.table(data_sepc, "../statistics/LSGs/data.spec", quote = F, sep = "\t", col.names = F, row.names = F)

hc <- hclust(dist(t(data_sepc[, c("Traes", "Hvul", "Scer", "Telo", "Astr", "Bdis", "Osat", "Zmay")])))
plot(hc)



# python#

df <- read.table("../statistics/LSGs/result", col.names = c("index", "num", "sumf", "tag"))
df <- table(df$tag)
names(df) <- c(
  "Traes.He.A", "Traes.He.B", "Traes.He.D",
  "Traes.Te.A", "Traes.Te.B",
  "Traes.Di.D", "Hvul", "Scer",
  "Telo", "Astr", "Bdis",
  "Osat", "Zmay"
)
df["Traes.Di.A"] <- 0

data <- data.frame(genome = names(df), lsgcs = as.numeric(df))
data$genome <- factor(data$genome, levels = c(
  "Traes.He.A", "Traes.He.B", "Traes.He.D",
  "Traes.Te.A", "Traes.Te.B", "Traes.Di.A",
  "Traes.Di.D", "Hvul", "Scer",
  "Telo", "Astr", "Bdis",
  "Osat", "Zmay"
))

ggplot(data, mapping = aes(x = genome, y = lsgcs, fill = genome)) +
  geom_bar(stat = "identity") +
  ggprism::theme_prism() +
  ylab("#Lineage-specific genes clusters") +
  xlab("") +
  # theme(axis.text.x = element_text(angle = 30, hjust = 1, vjust = 1)) +
  guides(fill = FALSE) + # 去除legend
  coord_flip()


# data_specSum$TraesASpec <- ifelse(data_spec$TraesA != 0 && sum(data_spec[, c(-1)]) == 0, "yes", "no")
# data_specSum$TraesBSpec <- ifelse(data_spec$TraesB != 0 && sum(data_spec[, c(-2)]) == 0, "yes", "no")
# data_specSum$TraesDSpec <- ifelse(data_spec$TraesD != 0 && sum(data_spec[, c(-3)]) == 0, "yes", "no")
# table(data_spec$TraesASpec)
# table(data_spec$TraesBSpec)
# table(data_spec$TraesDSpec)
#
sumMARY <- function(x) {
  df <- table(x)
  count_0 <- 34997 - as.numeric(df[names(df) == 0])
  return(count_0)
}

new_data <- data_size[, c(1:76)]
new_data[new_data == 1] <- 0

count <- apply(new_data, 2, sumMARY)

data <- data.frame(genome = names(count), lsgcs = as.numeric(count))
rownames(data) <- data[, 1]

df <- as.data.frame(t(data))[c(2), ]
data_all <- as.data.frame(lapply(df, function(x) as.numeric(as.character(x))))

data_all$Traes.He.A <- apply(data_all[, c(1:12)], 1, mean, na.rm = TRUE)
data_all$Traes.He.B <- apply(data_all[, c(13:24)], 1, mean, na.rm = TRUE)
data_all$Traes.He.D <- apply(data_all[, c(25:36)], 1, mean, na.rm = TRUE)
data_all$Traes.Te.A <- apply(data_all[, c(37:38)], 1, mean, na.rm = TRUE)
data_all$Traes.Te.B <- apply(data_all[, c(39:40)], 1, mean, na.rm = TRUE)
data_all$Traes.Di.A <- data_all[, 46]
data_all$Traes.Di.D <- apply(data_all[, c(41:45)], 1, mean, na.rm = TRUE)
data_all$Traes <- apply(data_all[, c(1:46)], 1, mean, na.rm = TRUE)
data_all$Hvul <- apply(data_all[, c(47:66)], 1, mean, na.rm = TRUE)
data_all$Scer <- apply(data_all[, c(67:68)], 1, mean, na.rm = TRUE)
data_all$Telo <- data_all[, 69]
data_all$Astr <- data_all[, 70]
data_all$Bdis <- data_all[, 71]
data_all$Osat <- apply(data_all[, c(72:74)], 1, mean, na.rm = TRUE)
data_all$Zmay <- apply(data_all[, c(75:76)], 1, mean, na.rm = TRUE)

data <- data.frame(genome = colnames(data_all[, -c(1:76, 84)]), lsgcs = as.numeric(data_all[, -c(1:76, 84)]))
data$genome <- factor(data$genome, levels = c(
  "Traes.He.A", "Traes.He.B", "Traes.He.D",
  "Traes.Te.A", "Traes.Te.B", "Traes.Di.A",
  "Traes.Di.D", "Hvul", "Scer",
  "Telo", "Astr", "Bdis",
  "Osat", "Zmay"
))


ggplot(data, mapping = aes(x = genome, y = lsgcs, fill = genome)) +
  geom_bar(stat = "identity") +
  ggprism::theme_prism() +
  ylab("#TDGCs") +
  xlab("") +
  # theme(axis.text.x = element_text(angle = 30, hjust = 1, vjust = 1)) +
  guides(fill = FALSE) + # 去除legend
  coord_flip()


## 计算 多倍化过程基因簇数量####

# ~ A ####
he_group <- c(1:12)
te_group <- c(37:38)
di_group <- c(46)

for (i in 1:length(new_data[, 1])) {
  he <- max(new_data[i, he_group])
  te <- max(new_data[i, te_group])
  di <- max(new_data[i, di_group])
  if (he * te * di > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/HE.txt", append = T, quote = F, row.names = F, col.names = F)
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/TE.txt", append = T, quote = F, row.names = F, col.names = F)
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/DI.txt", append = T, quote = F, row.names = F, col.names = F)
  } else if (he * te > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/HE.txt", append = T, quote = F, row.names = F, col.names = F)
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/TE.txt", append = T, quote = F, row.names = F, col.names = F)
  } else if (he * di > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/HE.txt", append = T, quote = F, row.names = F, col.names = F)
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/DI.txt", append = T, quote = F, row.names = F, col.names = F)
  } else if (te * di > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/TE.txt", append = T, quote = F, row.names = F, col.names = F)
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/DI.txt", append = T, quote = F, row.names = F, col.names = F)
  } else if (di > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/DI.txt", append = T, quote = F, row.names = F, col.names = F)
  } else if (te > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/TE.txt", append = T, quote = F, row.names = F, col.names = F)
  } else if (he > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/HE.txt", append = T, quote = F, row.names = F, col.names = F)
  }
}

# ~ B ####
bhe_group <- c(13:24)
bte_group <- c(39:40)

for (i in 1:length(new_data[, 1])) {
  he <- max(new_data[i, bhe_group])
  te <- max(new_data[i, bte_group])
  if (he * te > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/b.HE.txt", append = T, quote = F, row.names = F, col.names = F)
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/b.TE.txt", append = T, quote = F, row.names = F, col.names = F)
  } else if (te > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/b.TE.txt", append = T, quote = F, row.names = F, col.names = F)
  } else if (he > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/b.HE.txt", append = T, quote = F, row.names = F, col.names = F)
  }
}

# ~ D ####

dte_group <- c(25:36)
ddi_group <- c(41:45)

for (i in 1:length(new_data[, 1])) {
  te <- max(new_data[i, dte_group])
  di <- max(new_data[i, ddi_group])
  if (di * te > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/dd.DI.txt", append = T, quote = F, row.names = F, col.names = F)
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/dd.TE.txt", append = T, quote = F, row.names = F, col.names = F)
  } else if (te > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/dd.TE.txt", append = T, quote = F, row.names = F, col.names = F)
  } else if (di > 0) {
    write.table(rownames(new_data)[i], "../statistics/ABD_polyploidy/dd.DI.txt", append = T, quote = F, row.names = F, col.names = F)
  }
}

new_data <- read.table("../all_20211015/all_2021-10-15225207.size", header = T, row.names = 1)
save(new_data, file = "~/R/Rdata/all_size.RData")
load("~/R/Rdata/all_size.RData")

# ~* D Average ####
# for (group in 41:45) {
#   for (second_group in 25:36) {
#     dte_group <- second_group
#     ddi_group <- group
#     #assign(paste0("", i), i)
#     for (i in 1:length(new_data[, 1])) {
#       te <- max(new_data[i, dte_group])
#       di <- max(new_data[i, ddi_group])
#       if (di * te > 0) {
#         write.table(rownames(new_data)[i], paste0(out_dir,"all.DI.",group,"_",second_group,".txt"), append = T, quote = F, row.names = F, col.names = F)
#         write.table(rownames(new_data)[i], paste0(out_dir,"all.TE.",group,"_",second_group,".txt"), append = T, quote = F, row.names = F, col.names = F)
#       } else if (te > 0) {
#         write.table(rownames(new_data)[i], paste0(out_dir,"all.TE.",group,"_",second_group,".txt"), append = T, quote = F, row.names = F, col.names = F)
#       } else if (di > 0) {
#         write.table(rownames(new_data)[i], paste0(out_dir,"all.DI.",group,"_",second_group,".txt"), append = T, quote = F, row.names = F, col.names = F)
#       }
#     }
#   }
# }

GroupCompare <- function(rowData) {
  out_dir <- "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/statistics/ABD_polyploidy/"
  for (group in 41:45) {
    for (second_group in 25:36) {
      dte_group <- second_group
      ddi_group <- group
      te <- as.numeric(rowData[dte_group])
      di <- as.numeric(rowData[ddi_group])
      if (di * te > 0) {
        write.table(rowData[length(rowData)], paste0(out_dir,"fun.DI.", group, "_", second_group, ".txt"), append = T, quote = F, row.names = F, col.names = F)
        write.table(rowData[length(rowData)], paste0(out_dir,"fun.TE.", group, "_", second_group, ".txt"), append = T, quote = F, row.names = F, col.names = F)
      } else if (te > 0) {
        write.table(rowData[length(rowData)], paste0(out_dir,"fun.TE.", group, "_", second_group, ".txt"), append = T, quote = F, row.names = F, col.names = F)
      } else if (di > 0) {
        write.table(rowData[length(rowData)], paste0(out_dir,"fun.DI.", group, "_", second_group, ".txt"), append = T, quote = F, row.names = F, col.names = F)
      }
    }
  }
}

clu_data <- new_data
clu_data$clu <- rownames(clu_data)
clu_data[, c(1:76)] <- as.numeric(unlist(clu_data[, c(1:76)]))

tandem_clu_data <- data_size[, c(1:76)]
tandem_clu_data$clu <- rownames(tandem_clu_data)
tandem_clu_data[, c(1:76)] <- as.numeric(unlist(tandem_clu_data[, c(1:76)]))

apply(clu_data, 1, GroupCompare)
apply(tandem_clu_data, 1, GroupCompare)

tmp_df <- data.frame(group_one = c(), group_two = c(), dh = c(), d = c(), h = c())

out_dir <- "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/statistics/ABD_polyploidy/"

for (group in 41:45) {
  for (second_group in 25:36) {

    diploidD <- read.table(paste0(out_dir,"all.DI.", group, "_", second_group, ".txt"))[, 1]
    hexaploidD <- read.table(paste0(out_dir,"all.TE.", group, "_", second_group, ".txt"))[, 1]
    # 两个向量取交集
    dh <- length(intersect(x = diploidD, y = hexaploidD))
    
    d <- length(diploidD) - dh
    h <- length(hexaploidD) - dh

    df <- data.frame(group_one = c(group), group_two = c(second_group), dh = c(dh), d = c(d), h = c(h))
    tmp_df <- rbind(tmp_df, df)
  }
}

p5 <- tmp_df %>%
  gather(type, number, 3:5) %>%
  ggplot(aes(x = factor(type), fill = factor(type), y = number)) +
  geom_dotplot(binaxis = "y", stackdir = "center", position = "dodge") +
  ggprism::theme_prism()

p6 <- tmp_df %>%
  gather(type, number, 3:5) %>%
  ggplot(aes(x = factor(type), fill = factor(type), y = number)) +
  ylab("#Number") +
  xlab("") +
  scale_x_discrete(breaks = c("d", "dh", "h"), labels = c("Only Diploid D", "Both", "Only Hexaploid D")) +
  geom_dotplot(binaxis = "y", stackdir = "center", stackratio = .5, dotsize = .9) +
  ggprism::theme_prism() +
  theme(legend.position = "none") +
  geom_boxplot()


library(ggsignif) # 载入ggsignif

# 使用ggplot2包生成箱线图
P1 <- tmp_df %>%
  gather(type, number, 3:5) %>%
  ggplot(aes(x = factor(type), fill = factor(type), y = number)) + # ”fill=“设置填充颜色
  geom_jitter(aes(fill = type), width = 0.3, shape = 21, size = 2.5) + # 设置为向水平方向抖动的散点图，width指定了向水平方向抖动，不改变纵轴的值
  geom_boxplot(size = 0.5, alpha = 0.7, fill = "white", outlier.fill = "white", outlier.color = "white") + # size设置箱线图的边框线和胡须的线宽度，fill设置填充颜色，outlier.fill和outlier.color设置异常点的属性
  stat_boxplot(geom = "errorbar", width = 0.15, aes(color = "black")) + # 由于自带的箱形图没有胡须末端没有短横线，使用误差条的方式补上
  scale_fill_manual(values = c("#E69F00", "#0072B2", "#F0E442")) + # 设置填充的颜色
  scale_color_manual(values = c("black", "black", "black")) + # 设置散点图的圆圈的颜色为黑色
  scale_x_discrete(breaks = c("d", "dh", "h"), labels = c("Only Diploid", "Both", "Only Hexaploid")) +
  scale_y_continuous(breaks = c(5000,10000,15000,20000), labels = c("5k", "10k", "15k", "20k")) +
  # ggtitle("Car Milleage Data")+ #设置总的标题
  ggprism::theme_prism() + # 背景变为白色
  theme(
    legend.position = "none", # 不需要图例
    # axis.text.x=element_text(colour="black",family="Times",size=14), #设置x轴刻度标签的字体属性
    # axis.text.y=element_text(family="Times",size=14,face="plain"), #设置x轴刻度标签的字体属性
    # axis.title.y=element_text(family="Times",size = 14,face="plain"), #设置y轴的标题的字体属性
    # axis.title.x=element_text(family="Times",size = 14,face="plain"), #设置x轴的标题的字体属性
    # plot.title = element_text(family="Times",size=15,face="bold",hjust = 0.5), #设置总标题的字体属性
    # panel.grid.major = element_blank(), #不显示网格线
    panel.grid.minor = element_blank(),
    axis.text.x = element_text(angle = 30, hjust = 1, vjust = 1)
  ) +
  labs(y = "#Numbers", x = "") + # 设置x轴和y轴的标题
  geom_signif(
    comparisons = list(c("d", "h")),
    step_increase = 0.3,
    map_signif_level = T,
    test = wilcox.test
  )
P1

## 计算 多倍化过程基因簇数量####

#* 韦恩图 ----
library(VennDiagram)

for (group in 41:45) {

  # diploidD <- read.table(paste0("../statistics/ABD_polyploidy/dd.DI.",group,".txt"))[,1]
  # hexaploidD <- read.table(paste0("../statistics/ABD_polyploidy/dd.TE.",group,".txt"))[,1]

  diploidD <- read.table(paste0("../statistics/ABD_polyploidy/all.DI.", group, ".txt"))[, 1]
  hexaploidD <- read.table(paste0("../statistics/ABD_polyploidy/all.TE.", group, ".txt"))[, 1]

  # 二维韦恩图
  venn.plot <- venn.diagram(
    x = list("Diploid D" = diploidD, "Hexaploid D" = hexaploidD),
    filename = NULL,
    lwd = 4,
    fill = c("cornflowerblue", "darkorchid1"),
    alpha = 0.75,
    label.col = "white",
    cex = 3,
    fontfamily = "serif",
    fontface = "bold",
    cat.col = c("cornflowerblue", "darkorchid1"),
    cat.cex = 3,
    cat.fontfamily = "serif",
    cat.fontface = "bold",
    cat.dist = c(0.03, 0.03),
    cat.pos = c(-20, 14)
  )


  png(paste0("~/R/Rdata/all_venn", group, ".png"))
  grid.draw(venn.plot)
  dev.off()
}

#* 76都保留 ####
data_76 <- data_size[, 1:76]

data_76[data_76 < 3] <- 0
data_76[data_76 < 2] <- 0

data_76$clu3 <- apply(data_76, 1, prod, na.rm = TRUE)
clu3 <- rownames(dplyr::filter(data_76, clu3 > 0))

data_clu

#* ABD三套基因组 ####
ClassGroup <- c("Triplet", "Duplet", "Multiple", "Other", "Singleton") # 5:2:2:1

pan_ABD_df <- new_data[, c(1:12, 13:24, 25:36)]
pan_ABD_df$filter_tag <- apply(pan_ABD_df, 1, sum, na.rm = TRUE)
pan_ABD_df_filter <- dplyr::filter(pan_ABD_df, filter_tag > 0)[, -37]

pan_ABD_df_filter$pan_A_mean <- apply(pan_ABD_df_filter[, c(1:12)], 1, mean, na.rm = TRUE)
pan_ABD_df_filter$pan_B_mean <- apply(pan_ABD_df_filter[, c(13:24)], 1, mean, na.rm = TRUE)
pan_ABD_df_filter$pan_D_mean <- apply(pan_ABD_df_filter[, c(25:36)], 1, mean, na.rm = TRUE)

tmp_data <- ceiling(pan_ABD_df_filter[, c(37:39)])
cs_data <- new_data[, c(10, 22, 34)]
zang1817 <- new_data[, c(9, 21, 33)]
cs_data <- zang1817
cs_data$filter_tag <- apply(cs_data, 1, sum, na.rm = TRUE)
cs_data <- dplyr::filter(cs_data, filter_tag > 0)[, -4]

pan_A_group <- c(1:12)
pan_B_group <- c(13:24)
pan_D_group <- c(25:36)


check_type <- function(abd) {
  summaries <- table(abd)

  single_copy <- as.numeric(summaries[names(summaries) == "1"])
  pav <- as.numeric(summaries[names(summaries) == "0"])

  s <- length(single_copy)
  p <- length(pav)

  single_copy_num <- ifelse(s == 1, single_copy, 0)
  pav_num <- ifelse(p == 1, pav, 0)

  if (single_copy_num == 3) {
    type <- "Triplet"
  } else if (single_copy_num == 2) {
    if (pav_num == 1) {
      type <- "Duplet"
    } else {
      type <- "Multiple"
    }
  } else if (single_copy_num == 1) {
    if (pav_num == 2) {
      type <- "Singleton"
    } else {
      type <- "Other"
    }
  } else if (single_copy_num == 0) {
    type <- "Other"
  }
  return(type)
}

tmp_data$type <- apply(tmp_data, 1, check_type)
cs_data$type <- apply(cs_data, 1, check_type)

pie(table(tmp_data$type))
pie(table(cs_data$type))

#* 筛选所有基因组单拷贝 ####
tmp_data <- new_data
tmp_data$prod <- apply(tmp_data, 1, prod)
single_copy_data <- dplyr::filter(tmp_data, prod == 1)
write.table(rownames(single_copy_data), "../statistics/single_copy_cluid.txt", quote = F, col.names = F, row.names = F)

clu_file <- "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/all_2021-10-15225207.clu"
clu_size <- read.table(clu_file, header = T, row.names = 1)

save(clu_size, "~/R/Rdata/clu_size.RData")

write.table(clu_size[rownames(single_copy_data), ],
  "/data2/user2/xiexm/R/cooperation_in_lab/liubin/get_fa_by_gene_id/single_copy.clu",
  quote = F, sep = "\t"
)

#* 核心基因集合 ####
tmp_data <- data_all_size
tmp_data[tmp_data > 0] <- 1
tmp_data$filter_tag <- apply(tmp_data, 1, prod)
core_cluster <- dplyr::filter(tmp_data, filter_tag == 1)
tmp_data$core_tag <- apply(tmp_data, 1, sum)

library(tidyverse)
ggplot(tmp_data, aes(x = core_tag)) +
  geom_bar()

# 转换函数：比如y>1.3以上的都不想要了
trans <- function(x) {
  pmin(x, 25000) + 0.01 * pmax(x - 25000, 0)
}

# y坐标
yticks <- c(seq(0, 25000, by = 5000), 200000)

df <- data.frame(table(tmp_data$core_tag))
df$Frequency <- factor(c(1:76))
# 得到转换后的数据
df$Freq <- trans(df$Freq)
df$group <- c("Private", rep("Dispensable", 72), "Softcore", "Softcore", "Core")

ggplot(df, aes(x = Frequency, y = Freq, fill = group)) +
  geom_bar(stat = "identity") +

  # 在开始折叠处叠一个白的覆盖
  geom_rect(aes(xmin = 0, xmax = 6, ymin = 25000, ymax = 25500), fill = "white") +

  # 更新y坐标
  scale_y_continuous(limits = c(0, NA), breaks = trans(yticks), labels = c("0", "5k", "10k", "15k", "20k", "25k", "200k")) +
  scale_x_discrete(breaks = seq(5, 75, 5), labels = seq(5, 75, 5)) +

  # 假装折叠符号
  annotate("text", x = -1, y = 25500, colour = "black", label = ">", size = 10) +
  ylab("Cluster Number") +
  scale_fill_manual(values = c("#E36A71", "#609CD0", "#F9F3A9", "#E58E9E")) +
  ggprism::theme_prism()

percent_df <- df %>%
  group_by(group) %>%
  summarise(num = sum(Freq))

#* gggenes ####
library(gggenes)

genes <- read.table("/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/gggenes/bed/c_701.bed",
  col.names = c("molecule", "gene", "start", "end", "strand", "direction")
)

p4 <- ggplot2::ggplot(genes, ggplot2::aes(xmin = start, xmax = end, y = molecule, fill = molecule, label = gene)) +
  geom_gene_arrow(arrowhead_height = unit(3, "mm"), arrowhead_width = unit(1, "mm")) +
  geom_gene_label(align = "left") +
  scale_fill_brewer(palette = "Set3") +
  ggplot2::facet_wrap(~molecule, scales = "free", ncol = 1) +
  theme_genes() +
  theme(legend.position = "none") +
  ylab("Genomes")
