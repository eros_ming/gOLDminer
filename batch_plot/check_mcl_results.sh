#!/usr/bin/bash
I=$1
path=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/$2
prefix=`basename $path | cut -d '_' -f 2`
for gf in TaGRAS TaSPL MADS-box;do
	 while read line;do
		grep $line $path/IWGSCv1p1.I${I}0.tb | cut -f 2 >> ${gf}.${I}.${prefix}.QUERY
	 done < $gf
done
