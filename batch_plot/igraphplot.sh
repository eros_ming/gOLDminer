cd /data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/batch_plot
for GeneFam in TaGRAS MADS-box TaSPL;do
	while read line;do
	    #grep $line  /data/user/chenym/data_fordatabase/homolog_2021/IWGSCv1p1_IWGSCv1p1/IWGSCv1p1itself_IWGSCv1p1.one2many >> ${GeneFam}.grep
		#grep $line /data/user/chenym/data_fordatabase/homolog_witin_genome_b3/IWGSCv1p1_IWGSCv1p1/IWGSCv1p1itself_IWGSCv1p1.one2many >> ${GeneFam}.grep
		grep $line /data/user/chenym/data_fordatabase/homolog_whole_genome/homolog_witin_genome_nocol/IWGSCv1p1_IWGSCv1p1/IWGSCv1p1itself_IWGSCv1p1.one2many >> ${GeneFam}.grep
	done < ${GeneFam}
	
	 (cut -f 1 ${GeneFam}.grep;cut -f 2 ${GeneFam}.grep ) | sort | uniq > ${GeneFam}.grep2
	
	while read line;do
	    FIND_FILE=${GeneFam}
	    FIND_STR=$line
	    if [ `grep -c "$FIND_STR" $FIND_FILE` -ne '0' ];then
	       echo $line 'yes'
	    else
	        echo $line 'no'
	    fi
	done < ${GeneFam}.grep2 > ${GeneFam}.IF
	/usr/bin/Rscript 02.igraph.R $GeneFam
done
