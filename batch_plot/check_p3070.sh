#!/usr/bin/bash
wp=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/igraph_plot
outp=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/batch_plot
for gf in TaGRAS TaSPL MADS-box;do
	 while read line;do
		grep $line $wp/clusters.g.nocol | cut -f 2 >> $outp/${gf}.nocol.QUERY
		#grep $line $wp/clusters.g.p30 | cut -f 2 >> $outp/${gf}.p30.QUERY
		#grep $line $wp/clusters.g.p70 | cut -f 2 >> $outp/${gf}.p70.QUERY
	 done < $gf
done
