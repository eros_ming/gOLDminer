library(ggplot2)
library(grid)

kaks <- read.table("/data2/user2/xiexm/projs/ka_ks/ParaAT2.0/aet.kaks", header = T)


# 使用gather函数将宽数据gd1转换为长数据gd1_long
kaks_long <- tidyr::gather(kaks[, c("Ks", "Ka", "Ka.Ks")], type, value, c(Ks, Ka, Ka.Ks))
kaks_long$type <- factor(kaks_long$type, levels = c("Ka", "Ks", "Ka.Ks"))

kaks_new <- dplyr::filter(kaks_long, value <= 2.5)

compaired <- list(c("Ka", "Ks"))

p1 <- ggplot(kaks_new) +
  geom_boxplot(aes(x = type, y = value, fill = type)) +
  # ggsignif::geom_signif(comparisons = compaired,step_increase = 0.1,map_signif_level = F,test = t.test)
  ggprism::theme_prism() +
  xlab("") +
  ylab("")

kaks_class <- read.table(
  "/data2/user2/xiexm/projs/ka_ks/ParaAT2.0/aet.class",
  col.names = c(colnames(kaks), "Class"),
  fill = T
)

kaks_class$Class <- factor(kaks_class$Class,
  levels = c("tdg.pairs", "others"),
  labels = c("Tandem Gene Pairs", "Others")
)

# compaired <- list(c("tdg.pairs", "others"))


p3 <- ggplot(kaks_class) +
  geom_density(aes(Ka.Ks, fill = Class), alpha = 0.5) +
  ggprism::theme_prism() +
  xlab("Ka/Ks") +
  ylab("Density") +
  theme(
    legend.position = "top",
    legend.text = element_text(
      face = "bold",
      colour = "black",
      size = 12
    )
  )
p3
vie <- viewport(width = 0.4, height = 0.6, x = 0.8, y = 0.5)
p2 <- ggplot(kaks_class) +
  geom_boxplot(aes(x = Class, y = Ka.Ks, fill = Class)) +
  # ggsignif::geom_signif(comparisons = compaired,step_increase = 0.1,map_signif_level = F,test = t.test)
  ggprism::theme_prism() +
  ylab("Ka/Ks") +
  xlab("") +
  theme(
    axis.text.y = element_text(face = "bold", colour = "black"),
    axis.title.y = element_blank(),
    axis.text.x = element_text(face = "bold", colour = "black", size = 12),
    plot.background = element_rect(I(0), linetype = 0),
    panel.background = element_rect(I(0)),
    panel.grid.major = element_line(colour = NA),
    panel.grid.minor = element_line(colour = NA)
  ) +
  guides(fill = FALSE)
print(p2, vp = vie)

OutVals = boxplot(kaks_class$Ka.Ks)$out
kaks_class$out <- ifelse(kaks_class$Ka.Ks %in% OutVals,'yes','no')
kaks_class <- dplyr::filter(kaks_class,out == 'no')
