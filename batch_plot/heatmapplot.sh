#!/usr/bin/bash
gfLst=$1

all_clu_file=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/all_2021-10-15225207.clu
size=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/all_2021-10-15225207.size

/usr/bin/rm -rf /dev/shm/genefamily
mkdir -p /dev/shm/genefamily
for genefamily in `echo $gfLst| tr ',' ' '`;do
	while read line;do
	    grep $line $all_clu_file >> /dev/shm/genefamily/${genefamily}.clu
	done < /data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/batch_plot/$genefamily
	
	while read line;do
	    clu=`echo $line | awk '{print $1}'`;grep -w $clu $size >> /dev/shm/genefamily/${genefamily}.size
	done < /dev/shm/genefamily/${genefamily}.clu
	
	( head -n1 $size;sort /dev/shm/genefamily/${genefamily}.size | uniq ) > /dev/shm/genefamily/${genefamily}.sort.uniq.size
	
	/usr/bin/Rscript 01.pheatmap.R $genefamily
done