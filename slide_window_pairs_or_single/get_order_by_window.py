#!/usr/bin/env python
import sys

order_file = "./order.txt" #sys.argv[1]
with open(order_file) as odf:
	data = odf.readlines()
	order_dict = {}
	for line in data[1:]:
		dat = line.strip().split(" ")
		order_dict[dat[1]] = dat[0]


gene_order = "./IWGSCv1p1.gene.order"
with open(gene_order) as gof:
	data = gof.readlines()
	gene_order_dict = {}
	gene_order_dict_rev = {}
	for line in data:
		dat = line.strip().split(" ")
		gene, ind = dat
		gene_order_dict[gene] = ind
		gene_order_dict_rev[ind] = gene


single_gene_file = "./single.gene.list"
with open(single_gene_file) as sgl:
	data = sgl.readlines()
	gene_list_dict = {}
	for line in data:
		dat = line.strip().split(" ")
		cluid, gene = dat
		gene_list_dict[gene] = cluid


for key in gene_list_dict.keys():
	up_o_list = []
	down_o_list = []
	i, j = 1, 1
	o_now = order_dict.get(gene_list_dict[key],"-")
	exp = int(sys.argv[1])
	while len(up_o_list) < exp:
		order_now = gene_order_dict[key]
		i_pre = int(order_now) - i
		i += 1
		if i_pre == 0:
			break
		g_pre = gene_order_dict_rev[str(i_pre)]
		c_pre = gene_list_dict.get(g_pre,"NA")
		if c_pre == "NA":
			continue
		o_pre = order_dict.get(c_pre,"NA")
		if o_pre == "NA":
			continue
		up_o_list.append(o_pre)

	while len(down_o_list) < exp:
		order_now = gene_order_dict[key]
		i_nex = int(order_now) + j
		j += 1
		g_nex = gene_order_dict_rev.get(str(i_nex),"NA")
		if g_nex == "NA":
			break
		c_nex = gene_list_dict.get(g_nex,"NA")
		if c_nex == "NA":
			continue
		o_nex = order_dict.get(c_nex,"NA")
		if o_nex == "NA":
			continue
		down_o_list.append(o_nex)

	if len(up_o_list) > 0 and len(down_o_list) > 0:
		chrom = key[7:9]
		pre = " ".join(up_o_list)
		nex = " ".join(down_o_list)
		print(chrom, key, gene_list_dict[key], pre, o_now, nex)
