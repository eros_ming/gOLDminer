# -*- coding: utf-8 -*-
"""
@author: xiexm
@Date:  2021-11-29
@E-mail: erosminer@icloud.com
@Description: A script to extract sequences from fasta file.
"""
import sys


def usage():
    print('Usage: python3 script.py [clu_matrix] [out_path]')


def getBed(cluid, bed, gene, genome):
    dict = {}
    with open(bed, 'r') as bed_file:
        for line in bed_file:
            geneid = line.split('\t')[3]
            dict[geneid] = line.strip('\n')

    for g in gene:
        for key in dict.keys():
            if key == g:
                start = dict[key].split('\t')[1]
                end = dict[key].split('\t')[2]
                direct = dict[key].split('\t')[5]
                if direct == '+':
                    strand = 'forward'
                    direction = 1
                else:
                    strand = 'reverse'
                    direction = -1
                outfile.write(
                    f'{genome}\t{g}\t{start}\t{end}\t{strand}\t{direction}\n')


with open(sys.argv[1], 'r') as cluMatrix:
    clu_matrix = cluMatrix.read().split("\n")
    header = clu_matrix[0].split("\t")[1:]

    for row in clu_matrix[1:]:
        clu = row.split("\t")[1:]
        cluid = row.split("\t")[0]
        outfile = open(sys.argv[2]+'/'+cluid+'.bed', 'w')
        num = 0

        for genes in clu:
            if genes != ".":
                gene = genes.split(",")
                subgenome = header[num]
                genome = subgenome.split("_")[0]
                bed = f"/data2/user2/xiexm/projs/tandem_duplication/TGT/bed/{genome}.bed"
                getBed(cluid, bed, gene, subgenome)
            num += 1
        outfile.close()
