import sys
import os

out_dir = '/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/igraph_plot/'
# path_homo = "/data/user/chenym/data_fordatabase/homolog_2021/"
nodeinfo = open(out_dir+sys.argv[1]+'.nodeinfo', 'w')
# homoedgeinfo = open(out_dir+sys.argv[1]+'.homoedgeinfo', 'w')

with open('/dev/shm/genefamily/'+sys.argv[1]+'.sort.uniq.clu') as clu:
    data = clu.readlines()
    genome_list = data[0].strip().split('\t')[1:]
    for subdata in data[1:]:
        clu = subdata.strip().split('\t')
        cluid = clu[0]
        i = 0
        for subclu in clu[1:]:
            cluster = subclu.strip().split(',')
            for gene in cluster:
                if gene != '.':
                    nodeinfo.write(gene+'\t'+cluid+'\t'+genome_list[i]+'\n')
            i += 1
nodeinfo.close()
