library(datasets)
library(ggplot2)
library(ggsignif)

data(airquality)
airquality$Month <- factor(airquality$Month,
													 labels = c("May", "Jun", "Jul", "Aug", "Sep"))
airquality_trimmed <- airquality[which(airquality$Month == "Jul" |
																			 	airquality$Month == "Aug" |
																			 	airquality$Month == "Sep"), ]
airquality_trimmed$Temp.f <- factor(ifelse(airquality_trimmed$Temp > mean(airquality_trimmed$Temp), 1, 0),
																		labels = c("Low temp", "High temp"))

annot_1 <- wilcox.test(airquality_trimmed[airquality_trimmed$Month == "Jul" & airquality_trimmed$Temp.f == "High temp", "Ozone"],
											 airquality_trimmed[airquality_trimmed$Month == "Aug" & airquality_trimmed$Temp.f == "High temp", "Ozone"])$p.value

annot_2 <- wilcox.test(airquality_trimmed[airquality_trimmed$Month == "Jul" & airquality_trimmed$Temp.f == "Low temp", "Ozone"],
											 airquality_trimmed[airquality_trimmed$Month == "Aug" & airquality_trimmed$Temp.f == "Low temp", "Ozone"])$p.value

ggplot(airquality_trimmed, aes(x = Month, y = Ozone, fill = Temp.f)) +
	geom_boxplot(alpha=0.7) +
	geom_signif(annotations = c(formatC(annot_1, digits=3),formatC(annot_2, digits=3)),
							y_position = c(150, 180), xmin=c(1.2, 0.8), xmax=c(2.2, 1.8)) +
	scale_y_continuous(name = "Mean ozone in\nparts per billion",
										 breaks = seq(0, 175, 25),
										 limits=c(0, 175)) +
	scale_x_discrete(name = "Month") +
	ggtitle("Boxplot of mean ozone by month") +
	theme_bw() +
	theme(plot.title = element_text(size = 14, family = "Tahoma", face = "bold"),
				text = element_text(size = 12, family = "Tahoma"),
				axis.title = element_text(face="bold"),
				axis.text.x=element_text(size = 11),
				legend.position = "bottom") +
	scale_fill_brewer(palette = "Accent") +
	labs(fill = "Temperature") +
	ylim(NA, 190)
