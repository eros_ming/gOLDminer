#!/usr/bin/bash

#SBATCH --job-name=get_igraph_data
#SBATCH --nodes=1
#SBATCH --ntasks=20
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=20gb

gfLst=$1

all_clu_file=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/result_matrix/ABD_AB_A_D_20211015/ABD_AB_A_D_2021-10-15220437.clu
#all_clu_file=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/all_2021-10-15225207.clu
#all_clu_file=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/result_matrix/AetD_20211015/AetD_2021-10-15214715.clu #TODO: change ID QUER
block_path=/data/user/chenym/data_fordatabase/microcollinearity_file1
homo_path=/data/user/chenym/data_fordatabase/homolog_2021

/usr/bin/rm -rf /dev/shm/genefamily
mkdir -p /dev/shm/genefamily
for genefamily in `echo $gfLst| tr ',' ' '`;do
	while read line;do
	    grep $line $all_clu_file >> /dev/shm/genefamily/${genefamily}.clu
	done < /data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/batch_plot/$genefamily
	
	( head -n1 $all_clu_file;sort /dev/shm/genefamily/${genefamily}.clu | uniq ) > /dev/shm/genefamily/${genefamily}.sort.uniq.clu
	python /data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/igraph_plot/get_nodeinfo.py $genefamily

	for genome1 in `cut -f 3 ${genefamily}.nodeinfo | sort | uniq | tr '\n' ' '`;do
		for genome2 in `cut -f 3 ${genefamily}.nodeinfo | sort | uniq | tr '\n' ' '`;do
			block=$block_path/${genome1}.${genome2}.i1.blocks
			if [ -f $block ];then
				cat $block >> /dev/shm/genefamily/blocks
			fi
		done
	done

	while read line;do
		gene=`echo $line | cut -d' ' -f1`
		genome=`echo $line | cut -d' ' -f3 | cut -d'_' -f1`
		# echo $genome
		grep $gene /dev/shm/genefamily/blocks >> /dev/shm/genefamily/${genefamily}.para.edge
		grep $gene $homo_path/${genome}_${genome}/${genome}_${genome}itself.one2many >> /dev/shm/genefamily/${genefamily}.homo.edge
	done < ${genefamily}.nodeinfo
	
	(awk '{print $1,$2,"homo"}' /dev/shm/genefamily/${genefamily}.homo.edge | sort | uniq;
	awk '{print $0,"collinearity"}' /dev/shm/genefamily/${genefamily}.para.edge  | sort | uniq) > ${genefamily}.edge
done