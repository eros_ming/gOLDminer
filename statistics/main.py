#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File       :   main.py
@Time     :   2021/11/08 15:28:23
@Author  :   Xie Xiaoming
@Version  :   1.0
@Contact :   erosminer@icloud.com
@License :   (C)Copyright 2017-2021, All rights reserved
@Desc    :   None
'''

# here put the import lib
import sys
import numpy
from functools import reduce
# 定义变异系数计算函数


def coefficient_of_variation(data):
    mean = numpy.mean(data)  # 计算平均值
    std = numpy.std(data, ddof=0)  # 计算标准差
    cv = std/mean
    return cv


try:
    inputfile = sys.argv[1]
except IndexError:
    print('pls give me a Synteny Network Matrix(SNM) file.')

try:
    snm = open(inputfile).read().split('\n')
except IOError:
    print('pls give me the correct name for the Synteny Network Matrix(SNM) file')

genomeLst = snm[0].split('\t')[1:]
# group = {'L1': ['T093_ChrN', 'AY17_ChrN', 'XJ02_ChrN'],
#          'L2': ['AY61_ChrN', 'aet_ChrN']}

# group_index = {}

# for keys in group:
#     group_index[keys] = []
#     for genome in group[keys]:
#         group_index[keys].append(genomeLst.index(genome))

outfile = open('coefficient_variation_of_tandem_' + sys.argv[2], 'w')
outfile01 = open('size_01_' + sys.argv[2], 'w')
outfile11 = open('size_11_' + sys.argv[2], 'w')
outfile02 = open('size_02_' + sys.argv[2], 'w')
outfile12 = open('size_12_' + sys.argv[2], 'w')

num01 = num11 = num02 = num12 = 0
for line in snm[1:]:
    all_g = line.split('\t')[1:]
    num = [int(num) for num in all_g]
    try:
        product_of_num = reduce(lambda x, y: x*y, num)
        range_of_num = max(num) - min(num)
    except:
        break
    # TODO: handle 0/1  1/1 1/2+ 0/2+ 区分
    # p=1 -> 11
    # p=0 & r=1 -> 01
    # p=0 & r>1 -> 02
    # p>1 -> 12
    if product_of_num == 1:
        num11 += 1
        outfile11.write(line + '\n')
    elif product_of_num > 1:
        num12 += 1
        outfile12.write(line + '\n')
        outfile.write(line.split('\t')[0] + '\t' +
                      str(coefficient_of_variation(num)) + '\n')
    elif product_of_num == 0 and range_of_num == 1:
        num01 += 1
        outfile01.write(line + '\n')
    else:
        num02 += 1
        outfile02.write(line + '\n')
        outfile.write(line.split('\t')[0] + '\t' +
                      str(coefficient_of_variation(num)) + '\n')
    # print(coefficient_of_variation(num))
    # for grp in group_index:
    #     tmp_dict = []
    #     for ind in group_index[grp]:
    #         tmp_dict.append(num[ind])
    #     exec('{} = {}'.format(grp, tmp_dict))
    #     print(coefficient_of_variation(tmp_dict))
statistics = open('statistics_' + sys.argv[2], 'w')
statistics.write('singlegene_wo_pav\t'+str(num11)+'\n')
statistics.write('singlegene_wi_pav\t'+str(num01)+'\n')
statistics.write('tandemgene_wo_pav\t'+str(num12)+'\n')
statistics.write('tandemgene_wi_pav\t'+str(num02)+'\n')
statistics.write('tandemgene_percent\t' +
                 str(100*(num12+num02)/(num11+num01+num12+num02))+'\n')
statistics.write('pavgene_percent\t' +
                 str(100*(num01+num02)/(num11+num01+num12+num02))+'\n')
