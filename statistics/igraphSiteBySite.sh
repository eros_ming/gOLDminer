#!/usr/bin/bash

#SBATCH --job-name=igraphSiteBySite
#SBATCH --nodes=1
#SBATCH --ntasks=20
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=20gb

clu=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/all_2021-10-15225207.clu
cluid=$1
head -n 1 $clu > ${cluid}.clu
grep -w $cluid $clu > ${cluid}.clu.tmp
cat ${cluid}.clu ${cluid}.clu.tmp > ${cluid}.cluGroup
sed -i 's/\t/\n/g' ${cluid}.clu
sed -i '1d' ${cluid}.clu
sed -i 's/\t/\n/g' ${cluid}.clu.tmp
sed -i '1d' ${cluid}.clu.tmp

declare -i i=1
while read line;do
    for gene1 in `echo $line | tr ',' ' '`;do
         for gene2 in `echo $line | tr ',' ' '`;do
            echo $gene1 $gene2 $i >> ${cluid}.edge
         done
    done
    i+=1
done < ${cluid}.clu.tmp

(cut -d' ' -f 1,3 ${cluid}.edge ; cut -d' ' -f 2,3 ${cluid}.edge) | sort | uniq > ${cluid}.node
