#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File       :   FuncAnn
@Time     :   2021/12/28 22:12:31
@Author  :   Xie Xiaoming
@Version  :   1.0
@Contact :   erosminer@icloud.com
@License :   (C)Copyright 2017-2021, All rights reserved
@Desc    :   None
'''

# here put the import lib
import sys
from collections import defaultdict

go_list = "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/function_analysis/GO/goname.txt"

go_dict = {}
with open(go_list) as f:
    data = f.readlines()
    for record in data:
        description = record.split("\t")[0]
        accession = record.strip().split("\t")[1]
        go_dict[accession] = description

bio_pro = "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/function_analysis/GO/biological_process.txt"

gene_dict = {}
with open(bio_pro) as f:
    data = f.readlines()
    for record in data:
        gene = record.split("\t")[0]
        go = record.strip().split("\t")[1]
        gene_dict[gene] = go

# ann_dict = defaultdict(list)
# with open(go_list) as f:
#     data = f.readlines()
#     for record in data:
#         gene = record.split("\t")[0]
#         ann_dict[gene].append(" ".join(record.split("\t")[3:5]))

query_list = sys.argv[1]

with open(query_list) as f:
    data = f.readlines()
    num = 0
    for record in data:
        num += 1
        desc_list = []
        for gene in record.strip().split(","):
            try:
                go = gene_dict[gene]
                desc = go_dict[go]
                # print(gene+"\t"+str(num)+"\t" + go_dict[go])
                # print(gene+"\t"+str(num)+"\t" +
                #       " || ".join(list(set(gene_dict[gene]))))
            except KeyError:
                desc = "."
                # print(gene+"\t"+str(num)+"\t"+"null")
            #
            desc_list.append(desc)
        print(record.strip()+"\t"+str(num)+"\t" +
              " || ".join(list(set(desc_list))))
