#!/usr/bin/env Rscript
suppressPackageStartupMessages(library(clusterProfiler))
suppressPackageStartupMessages(library(stringr))
suppressPackageStartupMessages(library(optparse))
suppressPackageStartupMessages(library(xlsx))
option_list <- list(
  make_option(c("-f", "--foreground"),
    dest = "FgGene", default = "",
    help = "Foreground gene list file"
  ),
  make_option(c("-b", "--background"),
    dest = "BgGene", default = "",
    help = "foreground gene list file"
  ),
  make_option(c("-p", "--pvalue"),
    dest = "pvalue", default = "0.05",
    help = "pvalue cutoff"
  ),
  make_option(c("-m", "--padjust"),
    dest = "padjustmethod", default = "BH",
    help = "the method of padjust method"
  ),
  make_option(c("-s", "--species"),
    dest = "species", default = "IWGSCv1p1",
    help = "the name of species"
  ),
  make_option(c("-t", "--outtype"),
    dest = "outtype", default = "txt",
    help = "output type of file"
  ),
  make_option(c("-o", "--outenricher"),
    dest = "out", default = "out",
    help = "GO output file prefix"
  )
)
#
parser <- OptionParser(
  usage = "%prog [options] file",
  option_list = option_list, description = "Description:
                       GO analysis of wheat gene ( only BP) 2021.01.18\
                       Example: \
                       GO_script.R -f fggene.txt -b bggene.txt [ -p 0.05 ] [ -m BH  ] [ -s IWGSCv1.1 ] -o out.txt"
)
#
arguments <- parse_args(parser, positional_arguments = c(0, Inf))
fg <- arguments$options$FgGene
bggene <- arguments$options$BgGene
p <- arguments$options$pvalue
goadjust <- arguments$options$padjustmethod
out <- arguments$options$out
outtype <- arguments$options$outtype
spec <- arguments$options$species

GOFile <- "/data/user/chenym/shiny_TGT_database/GOdatabase/"

# background
if (bggene == "") {
  bggene <- read.table(paste0(GOFile, spec, "/all_gene.txt"), header = F, sep = "\t")
  colnames(bggene) <- "gene"
} else {
  bggene <- read.table(bggene, header = F, sep = "\t")
  colnames(bggene) <- "gene"
}

# forground genes #
fg <- read.table(fg, header = F, sep = "\t")
fggene <- as.data.frame(fg[, 1])
colnames(fggene) <- "gene"
fggene <- as.factor(fggene[, 1])

# goname #
GON <- paste0(GOFile, spec, "/goname.txt")
goname <- read.csv2(GON, sep = "\t", header = TRUE)
goname <- goname[, c(2, 1)]

BPN <- paste0(GOFile, spec, "/biological_process.txt")
biological_process <- read.csv2(BPN, sep = "\t", header = TRUE)
biological_process1 <- merge(bggene, biological_process, by.x = "gene", by.y = "Gene.stable.ID", all.x = TRUE)
biological_process1 <- na.omit(biological_process1)
biological_process1 <- biological_process1[, c(2, 1)]

### biological_process#####
x2 <- enricher(fggene,
  TERM2GENE = biological_process1, TERM2NAME = goname,
  pvalueCutoff = 1, qvalueCutoff = 1,
  minGSSize = 5, maxGSSize = 1200,
  pAdjustMethod = goadjust
)
biological <- data.frame()

if (nrow(as.data.frame(x2)) > 0) {
  x2@ontology <- "BP"
  x2@result <- x2@result[which(x2@result$p.adjust <= as.numeric(p)), ]
  x2o <- clusterProfiler::simplify(x2)
  biological <- as.data.frame(x2o)
  biological$group <- "biological_process"
}

# merge
a <- biological
names(a) <- c("ID", "Description", "GeneRatio", "BgRatio", "Pvalue", "Padjust", "qvalue", "GeneID", "Count", "Group")

#
a[, 5] <- signif(a[, 5], 3)
a[, 6] <- signif(a[, 6], 3)
a <- a[order(a[, 6], decreasing = F), ]
a <- a[which(a$Padjust <= as.numeric(p)), ]

# write
if (outtype == "txt") {
  write.table(a, paste0(out, "_GO.txt"), row.names = F, quote = F, sep = "\t")
} else {
  write.xlsx(a, paste0(out, "_GO.xlsx"),
    row.names = F,
    sheetName = "sheet", append = TRUE
  )
}
