#!/usr/bin/bash

#SBATCH --job-name=GO4MCL
#SBATCH --nodes=1
#SBATCH --ntasks=20
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=20gb
#set -euox pipefail

prefix=$1  #创建输出结果文件夹并设定输出文件前缀
mcl_cluster=$2  #文件绝对路径

work_path=$HOME/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/function_analysis/GO
results_path=$work_path/results_${prefix}_`date +%Y%m%d` 
go_process=$work_path/GO.R
tmp_dir=/dev/shm/go_tmp

mkdir -p $results_path
mkdir -p $tmp_dir

cd $tmp_dir
num=1
declare -i num
while read line;do
    echo $line > f.tmp
	sed -i 's/ /\n/g' f.tmp
	#file=`echo $line | tr '\t' '\n'`
	#echo $file > f.tmp
    first_gene=`echo $line | cut -d ' ' -f 1`
    #cat f.tmp
	/usr/bin/Rscript $go_process -f f.tmp -o $results_path/${num}_$first_gene
    #echo $go_process $first_gene
    num+=1
done < $mcl_cluster
