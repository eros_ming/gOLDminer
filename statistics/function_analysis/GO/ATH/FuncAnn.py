#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File       :   FuncAnn
@Time     :   2021/12/28 22:12:31
@Author  :   Xie Xiaoming
@Version  :   1.0
@Contact :   erosminer@icloud.com
@License :   (C)Copyright 2017-2021, All rights reserved
@Desc    :   None
'''

# here put the import lib
#import sys
from collections import defaultdict

go_list = "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/function_analysis/GO/ATH/ATH_GO"

ann_dict = defaultdict(list)
with open(go_list) as f:
    data = f.readlines()
    for record in data:
        gene = record.split("\t")[0]
        ann_dict[gene].append(" ".join(record.split("\t")[3:5]))

query_list = "/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/tb_file/ATH.CLU"

with open(query_list) as f:
    data = f.readlines()
    num = 0
    for record in data:
        num += 1
        for gene in record.strip().split(","):
            try:
                print(gene+"\t"+str(num)+"\t" +
                      " || ".join(list(set(ann_dict[gene]))))
            except KeyError:
                print(gene+"\t"+str(num)+"\t"+"null")
            #
