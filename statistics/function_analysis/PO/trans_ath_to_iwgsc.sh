#!/usr/bin/bash

#SBATCH --job-name=trans_ath_to_iwgsc
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=4
#SBATCH --mem=20gb

while read line;do
	iwgsc=`echo $line | awk '{print $1}'`
	ath=`echo $line | cut -f 2`
	po=`grep $ath /data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/function_analysis/PO/po_anatem | cut -d' ' -f 2`
	for p in `echo $po |tr '\n' ' '`;do
		( echo $iwgsc $p >> anotem_po ) &
	done
done < /data2/user2/xiexm/projs/tandem_duplication/TGT/homologs/IWGSCv1p1_ath/IWGSCv1p1_ath.one2one