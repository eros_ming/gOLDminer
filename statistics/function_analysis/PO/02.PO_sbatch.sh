#!/usr/bin/bash

#SBATCH --job-name=PO4MCL
#SBATCH --nodes=1
#SBATCH --ntasks=2
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=20gb
#set -euox pipefail

prefix=$1  #创建输出结果文件夹并设定输出文件前缀
mcl_cluster=$2  #文件绝对路径

work_path=$HOME/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/function_analysis/PO
results_path=$work_path/results_${prefix}
po_process=$work_path/PO.R
tmp_dir=/dev/shm/po_tmp

mkdir -p $results_path
mkdir -p $tmp_dir
cd $tmp_dir

# while read line;do
#     (
#     ftmp=`mktemp -u tmp.XXXXXX`
#     echo $line > $ftmp
# 	sed -i 's/ /\n/g' $ftmp
#     first_gene=`echo $line | cut -d ' ' -f 1`
# 	Rscript $po_process -f $ftmp -o $results_path/po_$first_gene
#     /usr/bin/rm  $ftmp
#     ) &
# done < $mcl_cluster

function PO {
    work_path=$HOME/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/function_analysis/PO
    results_path=$work_path/results_${prefix}
    po_process=$work_path/PO.R
    ftmp=`mktemp -u tmp.XXXXXX`
    echo $1 > $ftmp
	sed -i 's/ /\n/g' $ftmp
    first_gene=`echo $line | cut -d ' ' -f 1`
	Rscript $po_process -f $ftmp -o $results_path/po_$first_gene
    /usr/bin/rm  $ftmp
}
source $work_path/PO.sh

cat $mcl_cluster   | parallel --jobs 10 PO 
