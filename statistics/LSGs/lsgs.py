data = open('./data')
lineLst = data.read().split("\n")
N = 0
file = open("result", 'w')
for line in lineLst:
    subdata = line.split("\t")
    num = 0
    sumf = subdata[-1]
    N += 1
    for data in subdata:
        if data == sumf:
            num += 1
            file.write(str(N)+'\t'+str(data)+'\t'+str(sumf) +
                       '\t'+str(subdata.index(data))+'\n')
    if num == 0:
        file.write(str(N)+"\tNA\tNA\tNA"+'\n')
