gold_plot_3x <- function(data, colplot = F, pdf = F) {
	tryCatch({
		suppressMessages(library(ggplot2))
		suppressMessages(library(ggstar2))
		suppressMessages(library(ggtree))
		suppressMessages(library(ggpubr))
		suppressMessages(library(tidyr))
		suppressMessages(library(RColorBrewer))
		options(warn = -1)
		
		gg <- ggtree(ape::read.tree("/data2/user2/xiexm/Desktop/3x_tree_origin.nwk"))
		tree_df <- gg$data
		
		unique_data <- data[!duplicated(data[,c('cluid')]),]
		nameLst <- c()
		for (cluid in unique_data$cluid){
			new_name <- paste(data$gene_name[data$cluid == cluid],collapse = ";")
			nameLst <- append(nameLst,new_name)
			#unique_data[unique_data$cluid == cluid,]$gene_name <- new_name
		}
		unique_data$gene_name <- nameLst
		
		
		name <- as.character(unique_data[,17])
		#name <- unlist(strsplit(unlist(strsplit(as.character(data[,17]),"/")),";"))[1]
		
		df <- t(data.frame(unique_data[,1:10]))
		rownames(df) <- rownames(read.table("/data2/user2/xiexm/Desktop/may_tax.txt",
																				row.names = 1, header = TRUE
		))
		
		odls_tag <- unlist(strsplit(as.character(unique_data[,15]), "|", fixed = T))
		tag_l <- strsplit(odls_tag, ":")
		
		temp_df <- data.frame(
			odls = c(), node = c(),
			shape = c(), size = c(),
			color = c(),
			x = c(), y = c()
		)
		
		for (j in 1:ncol(df)){
			for (i in 1:4) {
				tag <- tag_l[[i+(j-1)*4]]
				if (length(tag) != 1 & tag[2] != "NA") {
					node_l <- as.numeric(unlist(strsplit(tag[2], ",")))
					shape <- c(21, 22, 4, 3)[i]
					size <- c(2, 1, 1, 0)[i]
					
					for (node in node_l) {
						node <- ifelse(node >= 20, 10, node)
						node <- ifelse(node == 19, 9, node)
						x <- tree_df$x[tree_df$node == node]
						y <- tree_df$y[tree_df$node == node]
						
						#col <- ifelse(node > 10,"grey","gray")
						col <- j
						cut <- ifelse(node > 10,.175*j,.175*j)
						
						tmp_df <- data.frame(
							odls = c(tag[1]),
							node = c(node),
							shape = c(shape),
							size = c(size),
							color = c(col),
							x = c(x - cut),
							y = c(y)
						)
						
						temp_df <- rbind(temp_df, tmp_df)
					}
				}
			}
			#off.set <- ifelse(temp_df$node[temp_df$odls == "O"] > 10,.1,.2)
		}
		
		legend_df <- data.frame(
			shape = c(21, 22, 4, 3),
			color = c(rep("grey", 4)),
			size = c(8,7,6,0),
			x = c(rep(-0.04, 4)),
			y = c(9, 8, 7, 6) + 1,
			note = c(
				"Origin", "Duplicate", "Loss",
				"Shrink"
			)
		)
		
		temp_df$color <- factor(temp_df$color)
		
		shrink_df <- dplyr::filter(temp_df,odls == "S")
		no_shrink_df <- dplyr::filter(temp_df,odls != "S")
		
		p <- gg +
			geom_tiplab(align = TRUE, linetype = "solid", fontface = "italic") +
			geom_point(
				data = no_shrink_df, aes(x, y, fill = no_shrink_df$color),
				shape = no_shrink_df$shape,
				size = no_shrink_df$size, stroke = 1
			) +
			geom_star(data = shrink_df, 
								aes(x, y, fill = shrink_df$color),
								size = 1,
								color = "black",
								starshape = 31,
								starstroke = 1) +
			geom_point(
				data = legend_df, aes(x, y),
				shape = legend_df$shape,
				fill = legend_df$color, size = legend_df$size, stroke = 1
			) +
			geom_star(aes(-0.04, 7), 
								size = 5,
								color = "black",
								fill = "grey",
								starshape = 31,
								starstroke = 1) +
			geom_text(
				data = legend_df,
				aes(x + 0.65, y, label = note),
				fontface = "bold", hjust = 0
			) #+ geom_text(aes(-0.04,10, label = data[14]), fontface = "bold", hjust = 0)
		
		p$data$label <- c("Z. mays","O. sativa","B. distachyon",
											"A. strigosa","H. vulgare","S. cereale","Th. elongatum",
											"D lineage","B lineage","A lineage"
											,rep(NA,8))
		
		##* ggplot2 画热图####
		
		df2 <- data.frame(df)
		colnames(df2) <- unique_data$cluid
		df2$Species <- rownames(df2)
		df2$Species <- factor(df2$Species, levels = rev(rownames(df2)))
		
		df3 <- gather(df2,cluID,hocSize,colnames(df2)[1:ncol(df)])
		
		max_size <- max(df3$hocSize)
		
		if (max_size < 11) {
			color_list <- colorRampPalette(brewer.pal(9, "YlOrRd"))(10)
		} else {
			color_list <- colorRampPalette(brewer.pal(9, "YlOrRd"))(max_size)
		}
		transColor <- function(data) {
			size <- as.numeric(data[3])
			color <- ifelse(size == 0, "grey", color_list[size])
			return(color)
		}
		df3$Type <- apply(df3, 1, transColor)
		cf <- unique_data$cluid
		## 按cluid编号升序排列
		#df3$cluID <- factor(df3$cluID,levels = cf[order(nchar(cf),cf)])
		## 或者按用户输入顺序排列
		df3$cluID <- factor(df3$cluID,levels = cf)
		#gene_name <- ifelse(unique_data[,16] != "",as.character(unique_data[,16]),"")
		
		p1 <- ggplot(df3, aes(cluID, Species)) +
			geom_tile(fill = df3$Type, colour = "black") +
			geom_text(aes(label = hocSize), col = "black", size = 5) +
			theme_bw() +
			xlab("") +
			ylab("") +
			ggtitle("") +
			scale_y_discrete(position = "right") + 
			scale_x_discrete(position = "top", labels = unique_data$gene_name) +
			theme(
				#axis.title.y = element_text(color = "grey"),
				#axis.title.x = element_text(color = "blue",face = "italic"),
				axis.text.x = element_text(color = "blue",face = "italic",
																	 angle = 30, hjust = 0, vjust = 1),
				axis.text.y = element_blank(),
				axis.ticks = element_blank(),
				panel.border = element_blank(),
				panel.grid.major = element_blank(),
				panel.grid.minor = element_blank(),
				legend.position = "none")
		
		p2 <- p + xlim(-1, 12.5)
		p3 <- ggarrange(p2, p1,widths = c(5, 1), align = "h")
		
		if (colplot == T) {
			suppressMessages(library(readr))
			suppressMessages(library(gggenomes))
			
			##* gggenomes 画共线性图####
			cluid <- data[14]
			#work_dir <- "/data2/user2/xiexm/projs/GeneODL/ODLSplot/collinearity/"
			work_dir <- "/data2/user2/xiexm/projs/GeneODL/know_cluster_or_family/tmp2/"
			
			genes_tbl <- read_table(paste0(work_dir, cluid, ".genes"),col_names = F)
			colnames(genes_tbl) <- c("seq_id","start","end","strand","width","source","type","feature_id","name")
			
			link <- read_table(paste0(work_dir, cluid, ".links"),col_names = F)
			colnames(link) <- c("name","name2","seq_id","seq_id2")
			
			tbl <- genes_tbl[c("name","start","end","strand")]
			part_link1 <- dplyr::left_join(link, tbl)
			
			tbl_rev <- genes_tbl[c("name","start","end","source")]
			colnames(tbl_rev) <- c("name2","start","end","source")
			part_link2 <- dplyr::left_join(link, tbl_rev)
			
			links_tbl_tmp <- dplyr::left_join(part_link1, part_link2, by = "name")
			links_tbl_tmp$length <- abs(links_tbl_tmp$end.x - links_tbl_tmp$start.x)
			links_tbl_tmp$length2 <- abs(links_tbl_tmp$end.y - links_tbl_tmp$start.y)
			links_tbl <- links_tbl_tmp[c(3,13,5:7,10,13,11:12)]
			links_tbl <- links_tbl_tmp[c(3,14,5:7,10,15,11:12,13)]
			colnames(links_tbl) <- c("seq_id", "length", "start", "end", "strand",
															 "seq_id2", "length2", "start2", "end2","td")
			
			#TODO
			transGenes2seqs <- function(data){
				h76 <- "/data2/user2/xiexm/projs/GeneODL/data/76.group"
				h <- read.table(h76, header = T)
				lst = list()
				for (genomes in names(table(data$seq_id))) {
					lst[[genomes]]["start"] = min(dplyr::filter(data, seq_id == genomes)[,c(2,3)])
					lst[[genomes]]["end"] = max(dplyr::filter(data, seq_id == genomes)[,c(2,3)])
					lst[[genomes]]['len'] = lst[[genomes]]["end"] - lst[[genomes]]["start"]
					lst[[genomes]]['group'] = h[h$subgenome == genomes,]$group
					lst[[genomes]]['order'] = h[h$subgenome == genomes,]$order
				}
				
				for (g in unique(h$group)) {
					if (!g %in% unlist(lst)) {
						genomes <- as.character(h[h$group == g,]$subgenome[1])
						order <- h[h$group == g,]$order[1]
						lst[[genomes]]['start'] = 10
						lst[[genomes]]['end'] = 30
						lst[[genomes]]['len'] = 20
						lst[[genomes]]['group'] = g
						lst[[genomes]]['order'] = order
					}
				}
				
				df <- data.frame(matrix(unlist(lst),
																nrow = 10,
																byrow = T),
												 stringsAsFactors = FALSE)
				
				colnames(df) <- c('s','e','l','group','order')
				
				df$genomes <- h$subgenome[c(as.numeric(df$order))]
				seqs_tbl <- tibble(
					bin_id = df$group,
					seq_id = df$genomes,
					length = as.numeric(df$l)
				)
				rownames(seqs_tbl) <- seqs_tbl$bin_id
				return(seqs_tbl[unique(h$group),])
			}
			
			seqs_tbl <- transGenes2seqs(genes_tbl)
			
			p4 <- gggenomes(seqs = seqs_tbl,
											genes = genes_tbl,
											links = links_tbl) +
				geom_seq() +
				geom_seq_label() +   # label each sequence
				# geom_bin_label() + # label each bin
				geom_link(aes(fill = td), offset = 0.05, inherit.aes = F) +
				geom_gene(aes(fill = source)) +
				scale_fill_manual(values = c("NotHoG" = "#c1c1c1",
																		 "HoGS" = "#f33c25")) +
				# geom_gene_tag(aes(label = name),
				# 							nudge_y = 0.1,
				# 							check_overlap = TRUE) +
				theme(axis.line.x = element_blank(),
							axis.ticks = element_blank(),
							axis.text.x = element_blank(),
							legend.position = "none")
			
			## 拼接图片 ####
			
			out <- ggarrange(p3, p4,widths = c(6, 3), align = "h")
		}else{
			out <- p3
		}
		
		if (pdf == T) {
			pdf(file = paste("/data2/user2/xiexm/Desktop/knGene/v5/",
											 data[12], "-", data[13], "-", name, ".pdf",
											 sep = ""
			), height = 4, width = 7)
			print(out)
			dev.off()
		}else{
			out
		}
		
	}, error = function(e) {
		message(paste0("error:",data[14]))
	})
}