#!/usr/bin/bash

#SBATCH --job-name=GOmuSample
#SBATCH --nodes=1
#SBATCH --ntasks=5
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=2gb
# 18 IWGSCv1p1 11,23,35 /data/index_new/thinopyrum_elongatum/blast/thinopyrum_elongatum
# 17 te 70

while read node spec NR;do

	while read cluid onode;do
        rec=`grep -w $cluid /data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/all_2021-10-15225207.clu | awk  '{print $11,$23,$35}'`
        echo $cluid"\t"$rec
    done < ${spec}_spec_HoC.txt > ${spec}_spec_CS_geneid.txt
	
	sed -i 's/\\t/\t/g' ${spec}_spec_CS_geneid.txt
    
    echo $spec > ${spec}_spec_CS_geneid_clean.txt
	python3 ../cleandata.py ${spec}_spec_CS_geneid.txt | awk '{print $2}' >> ${spec}_spec_CS_geneid_clean.txt

done < batch_find_homo.txt
