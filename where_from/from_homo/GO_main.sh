#/usr/bin/bash 
#SBATCH --job-name=GO_where_from
#SBATCH --nodes=1
#SBATCH --ntasks=8
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=50gb
#set -euox pipefail

work_path=/data2/user2/xiexm/projs/GeneODL/where_from/from_homo
go_process=$work_path/GO/GO.R
background=$work_path/GO/bg_gene.txt

while read node spec NR;do
	/usr/bin/Rscript $go_process -f $background -o "$node".txt
done < batch_find_homo.txt
