#!/usr/bin/bash

#SBATCH --job-name=exonerate
#SBATCH --nodes=1
#SBATCH --ntasks=16
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=20gb
module load exonerate/2.2.0

work_path=/data2/user2/xiexm/projs/GeneODL/where_from/exonerate
for id in `echo a{k..z}`;do
    exonerate -t $work_path/fa/the/the.fa -q $work_path/10/nothomo_"$id" --model protein2genome --showtargetgff > $work_path/out/nothomo_"$id".exonerate.out &
done
wait
