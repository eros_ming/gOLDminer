#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File       :   grep
@Time     :   2022/04/18 01:56:13
@Author  :   Xie Xiaoming
@Version  :   1.0
@Contact :   erosminer@icloud.com
@License :   (C)Copyright 2017-2021, All rights reserved
@Desc    :   None
'''

# here put the import lib
import sys

to_spec = sys.argv[1]
# homo_file = "/data/user/chenym/data_fordatabase/homolog_2021/IWGSCv1p1_" + to_spec + "/IWGSCv1p1_" + to_spec + ".one2one"
homo_file = "/data/user/chenym/data_fordatabase/homolog_2021/" + to_spec + "_IWGSCv1p1/" + to_spec + "_IWGSCv1p1.one2many"

homo_dict = {}
with open(homo_file) as f:
    data = f.readlines()
    for line in data:
        rec = line.strip().split("\t")
        cs = rec[0]
        homo_dict[cs] = rec[1]

with open("tri_spec_HoC_geneid_clean.txt") as f:
    data = f.readlines()
    for line in data:
        rec = line.strip().split("\t")
        cluid = rec[0]
        gene = rec[1]
        # if gene in homo_dict.keys():
        if gene in homo_dict.values():
            print(cluid +"\t" + gene + "\t" + "homo")
        else:
            print(cluid +"\t" + gene + "\t" + "nothomo")
