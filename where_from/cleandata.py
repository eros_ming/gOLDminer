#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File       :   cleandata
@Time     :   2022/04/18 01:56:13
@Author  :   Xie Xiaoming
@Version  :   1.0
@Contact :   erosminer@icloud.com
@License :   (C)Copyright 2017-2021, All rights reserved
@Desc    :   None
'''

# here put the import lib
import sys
import re

input_file = sys.argv[1]

with open(input_file) as f:
    data = f.readlines()
    for line in data:
        rec = line.strip().split("\t")
        cluid = rec[0]
        genelst = re.split(" |,", rec[1])
        for gene in genelst:
            if gene != ".":
                print(cluid +"\t" + gene)
