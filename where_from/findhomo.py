#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File       :   grep
@Time     :   2022/04/18 01:56:13
@Author  :   Xie Xiaoming
@Version  :   1.0
@Contact :   erosminer@icloud.com
@License :   (C)Copyright 2017-2021, All rights reserved
@Desc    :   None
'''

# here put the import lib
import sys

def get_keys(d, value):
    return [k for k,v in d.items() if v == value]

spec = sys.argv[1]
homo_file = "/data/user/chenym/data_fordatabase/homolog_2021/" + spec + "_" + spec +"/" + spec + "itself_" + spec +".one2many"
# homo_file = "/data/user/chenym/data_fordatabase/homolog_2021/IWGSCv1p1_IWGSCv1p1/IWGSCv1p1itself_IWGSCv1p1.one2many"

homo_dict = {}
with open(homo_file) as f:
    data = f.readlines()
    for line in data:
        rec = line.strip().split("\t")
        cs = rec[0]
        homo_dict[cs] = rec[1]

with open(spec + "_spec_HoC_geneid_clean.txt") as f:
    data = f.readlines()
    cluidlst = {}
    for line in data:
        rec = line.strip().split("\t")
        cluid = rec[0]
        gene = rec[1]
        if cluid not in cluidlst.keys():
            cluidlst[cluid] = [gene]
        else:
            cluidlst[cluid].append(gene)

    for cluid in cluidlst.keys():
        genelst = cluidlst[cluid]
        i = 0

        homolst =[]
        for gene in genelst:
        # if gene in homo_dict.keys():
            if gene in homo_dict.values() or gene in homo_dict.keys():
                if homo_dict[gene] not in genelst and get_keys(homo_dict,gene) not in genelst:
                    
                    i += 1
                    if gene in homo_dict.values():
                        homolst.append(get_keys(homo_dict,gene))
                    else:
                        homolst.append(homo_dict[gene])
            #     else:
            #         print(cluid +"\t" + gene + "\t" + "nothomo")
            # else:
            #     print(cluid +"\t" + gene + "\t" + "nothomo")
            
        if i > 0:
            print(cluidlst[cluid][0])

