# 合并两个列名相同但乱序文件 ####
args <- commandArgs(T)

h76 <- "/data2/user2/xiexm/projs/GeneODL/data/76.group"
h <- read.table(h76,header = T)
head <- c("clu",as.character(h$subgenome))

data_head <- data.table::fread(args[1], header = T)
data_tail <- data.table::fread(args[2], header = T)

combined_dt <- rbind(as.data.frame(data_head)[,head],
										 as.data.frame(data_tail)[,head])

write.table(combined_dt,
						args[3],
						quote = F,
						sep = '\t',
						col.names = T,
						row.names = F)