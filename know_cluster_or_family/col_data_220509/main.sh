#!/bin/bash

#SBATCH --job-name=get_G&L
#SBATCH --partition=wggc1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --output=%j.out
#SBATCH --error=%j.err
while read geneid genename cluid;do
	python3 /data2/user2/xiexm/projs/GeneODL/know_cluster_or_family/get_links.py $cluid 2 /dev/shm/tmp.clu /dev/shm/tmp.size > "$cluid".links
	python3 /data2/user2/xiexm/projs/GeneODL/know_cluster_or_family/get_genes.py $cluid 2 /dev/shm/tmp.clu /dev/shm/tmp.size 10 5 > "$cluid".genes
done < /data2/user2/xiexm/projs/GeneODL/know_cluster_or_family/knGwithCluID.txt
