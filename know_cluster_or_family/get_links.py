#!/usr/bin/env python3
import sys
import os

cluid = sys.argv[1]
expansion = sys.argv[2]
clufile = sys.argv[3]
sizefile = sys.argv[4]

datadir = "/data2/user2/xiexm/Desktop/knGene/76_220508/"
clu = datadir + "all_2022-05-08224817.clu"
size = datadir + "all_2022-05-08224817.size"


def get_bed(spec,gene_list,expansion):
    data_path = "/data/user/chenym/data_fordatabase/rawdata/"
    bed = data_path + spec.split("_")[0] +".bed"
    tmp_dict = {}
    with open(bed) as bed_file:
        gene_bed = bed_file.readlines()
        i = 0
        gene_lst = []
        order_lst =[]
        for g in gene_bed:
            bed_record = g.strip().split("\t")
            gene = bed_record[3]
            gene_lst.append(gene)
            if gene in gene_list:
                order_lst.append(i)
            sub_tmp_dict ={}
            sub_tmp_dict["spec"] = spec
            sub_tmp_dict["chr"] = bed_record[0]
            sub_tmp_dict["start"] = bed_record[1]
            sub_tmp_dict["end"] = bed_record[2]
            sub_tmp_dict["strand"] = bed_record[5]
            tmp_dict[gene] = sub_tmp_dict
            i += 1

        if len(order_lst) >= 1:
            min_ind = min(order_lst) - int(expansion)
            max_ind = max(order_lst) + int(expansion) + 1
            new_gene_lst = gene_lst[min_ind:max_ind]
        else:
            new_gene_lst = []
        
        return(new_gene_lst)



def get_link(from_spec, to_spec, from_gene_lst, to_gene_lst):
    data_path = "/data/user/chenym/data_fordatabase/microcollinearity_file1/" 
    blocks = data_path + from_spec + '.' + to_spec + '.i1.blocks'
    collinearity_dict = {}
    with open(blocks) as block_file:
        data = block_file.readlines()
        for line in data:
            pairs = line.strip().split("\t")
            collinearity_dict[pairs[0]] = pairs[1]
    links = {}
    for gene in from_gene_lst:
        try:
            to_gene = collinearity_dict[gene]
        except KeyError:
            to_gene = '.'
        if to_gene in to_gene_lst:
            links[gene] = to_gene
    return(links)


cmd = '(head -n 1 ' + clu + ';grep -w ' + cluid + ' ' + clu + ') > ' + clufile
os.system(cmd)

cmd2 = 'grep -w ' + cluid + ' ' + size + ' > ' + sizefile
os.system(cmd2)

def get_max_size_index(sizefile):
    a1 = list(range(1,13))
    a2 = list(range(37,39))
    a3 = [46]
    a = a1 + a2 + a3
    b1 = list(range(13,25))
    b2 = list(range(39,41))
    b = b1 + b2
    d1 = list(range(25,37))
    d2 = list(range(41,46))
    d = d1 + d2
    Telo = [69]
    Scer = list(range(67,69))
    Hvul = list(range(47,67))
    Astr = [70]
    Bdis = [71]
    Osat = list(range(72,75))
    Zmay = list(range(75,77))

    group = {1:a, 2:b, 3:d, 4:Telo, 5:Scer, 6:Hvul, 7:Astr, 8:Bdis, 9:Osat, 10:Zmay}
    spec_order_list = []
    with open(sizefile) as sf:
        clu_size = sf.readline().strip().split("\t")
        for  gr in group.keys():
            index = group[gr]
            clu_s = []
            for ind in index:
                clu_s.append(clu_size[ind])
            ind2 = clu_s.index(max(clu_s))
            spec_ind = group[gr][ind2]
            spec_order_list.append(spec_ind)
    return(spec_order_list)


# #spec_list = []
# spec_order_list = list(range(1, 77)) # [10,22,34,69,68,50,70,71,74,76] # range(77)
spec_order_list = get_max_size_index(sizefile)

with open(clufile) as f:
    spec = f.readline().strip().split("\t")
    clusters = f.readline().strip().split("\t")
    gene_lst_dict = {}
    spec_list = spec[1:]

    for ogs in spec_order_list:
        sub_spec = spec[ogs]
        gene_list = clusters[ogs].split(",")
        new_gene_lst =  get_bed(sub_spec, gene_list, expansion)
        gene_lst_dict[sub_spec] = new_gene_lst
    a = [i - 1 for i in spec_order_list][1:]
    b = [i - 1 for i in spec_order_list][:-1]
    for i in zip(a,b):
        from_spec = spec_list[i[1]]
        to_spec = spec_list[i[0]]
        from_gene_lst = gene_lst_dict[from_spec]
        to_gene_lst = gene_lst_dict[to_spec]
        links = get_link(from_spec, to_spec, from_gene_lst, to_gene_lst)
        for k in links:
            seq_id = k
            seq_id2 = links[k]
            print(seq_id,seq_id2,from_spec,to_spec)
