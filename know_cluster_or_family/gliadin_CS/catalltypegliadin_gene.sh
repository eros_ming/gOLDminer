#!/bin/bash
set -euxo pipefail

for type in alpha avenin delta gamma HMW LMW omega;do
    cut -f 5,6,7,8 /data2/user2/xiexm/R/GeneCluscd ter/pipeline/00.rebuild/backbone/TDGproj_20210821/gliadin_CS/CS_gapless/${type}.result| awk -F'-' '{print $1,$3}' | uniq >> all._type_gliadin_gene.info
done

while read line;do
grep $line ../all_20211015/all_2021-10-15225207.clu >> alltype.gliadin.clu                                        
done < gliadin.
while read line;do             
clu=`echo $line | awk '{print $1}'`;grep -w $clu ../all_20211015/all_2021-10-15225207.chr >> gliadin.chr
done < gliadin.sorteduniq.clu