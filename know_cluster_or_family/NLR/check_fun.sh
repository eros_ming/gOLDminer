#!/usr/bin/bash

#SBATCH --job-name=Untitled-1
#SBATCH --nodes=1
#SBATCH --ntasks=20
#SBATCH --partition=wggc1
#SBATCH --cpus-per-task=1
#SBATCH --mem=20gb
file_two=$1
clufile=/data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/TDGproj_20210821/all_20211015/all_2021-10-15225207.clu
echo "###############gliadin################"
file_one=/data2/user2/xiexm/projs/tandem_duplication/TGT/gliadin.list
#比较两个文件相同内容
awk  'NR==FNR{a[$0]}NR>FNR{ if($0 in a) print $0}' $file_one $file_two > gene.list.tmp

while read line;do
    clu=`grep $line $clufile | awk '{print $1}'`
    echo $line $clu
done < gene.list.tmp

echo "###############NLR################"
file_one=//data2/user2/xiexm/R/GeneCluster/pipeline/00.rebuild/backbone/NLR/nlr_gene.id
#比较两个文件相同内容
awk  'NR==FNR{a[$0]}NR>FNR{ if($0 in a) print $0}' $file_one $file_two > gene.list.tmp

while read line;do
    clu=`grep $line $clufile | awk '{print $1}'`
    echo $line $clu
done < gene.list.tmp