#NLR 饱和曲线####
nlrGene <- read.table("./00.rebuild/backbone/NLR/IWGSCv1p1.nlrdb",
                      col.names = c("chr","start","end","gene","cluID","chain","order","note"))

barplot(table(nlrGene$chr))

pie(table(nlrGene$note))

node <- unique(nlrGene$cluID)

geneBackbone <- read.table("./00.rebuild/backbone/14A.matrix",
                           header = T)

nlrBackbone <- geneBackbone$clu[geneBackbone$IWGSCv1p1_chrNA%in%node]

write(as.character(nlrBackbone),file = "./00.rebuild/backbone/NLR/nodelist")

nlrBackbone <- read.table("./00.rebuild/backbone/NLR/14A.nlr.matrix")

for (i in 2:length(colnames(geneBackbone))) {
  cat(colnames(geneBackbone)[i],"\t",table(nlrBackbone[,i])[1],"\n")
}

pav <- read.table("./00.rebuild/backbone/NLR/14A.pav.nlr",col.names = c("gm","pavN"))

pav

pav$N <- 583 - pav$pavN

plot(sort(c(pav$N,583))) #错误的方法

sampleNLR <- function(samplematrix,num) {
  gmN <- length(colnames(samplematrix))
  df <- data.frame(num = c(0),gmNum = c(0))
  for (i in 1:gmN) {
    for (j in 1:num) {
      data <- samplematrix[,sample(1:13,i)]
      d <- matrix(data = as.numeric(data), nrow = 583, ncol = i, byrow = FALSE, dimnames = NULL)
      nlrnum <- 583 - as.numeric(table(apply(d, 1, sum))[1])
      dfAdd <- data.frame(num = c(nlrnum),gmNum = c(i))
      df <- rbind(df,dfAdd)
    }
  }
  return(df)
}

nlrmatrix <- as.matrix(nlrBackbone[,-1])
nlrmatrix[nlrmatrix != "."] <- 1
nlrmatrix[nlrmatrix == "."] <- 0
samplematrix <- nlrmatrix[,-2]

test <- sampleNLR(samplematrix,100)

boxplot(df$gmNum,df$num)

e1 <- ggplot(test, aes(x = gmNum, y = num)) + 
  #geom_smooth() + #绘制平滑曲线
  #geom_jitter(width =0.2,shape = 21,size=1) +  #添加散点
  geom_boxplot(aes(group = gmNum), position=position_dodge(0.5),width = 0.1) + 
  ylim(c(0,580)) + 
  stat_summary(fun = median, geom = "line", aes(group = 1), color = "blue", size = 2)  +
  stat_summary(fun = median, geom = "point") +
  xlab("#Number of Genomes") + ylab("#Number of NLR Genes")+
  theme_classic() + 
  theme(axis.title = element_text(size = 15, face = "bold"),
        axis.text = element_text(size = 15, face = "bold")) +
  scale_x_continuous(breaks = seq(0, 13, 1))
e1

ggsave("./00.rebuild/backbone/NLR/NLR_saturation_curve.pdf")
